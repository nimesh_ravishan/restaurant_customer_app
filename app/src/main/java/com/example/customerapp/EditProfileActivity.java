package com.example.customerapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.loader.content.CursorLoader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.customerapp.Model.Customer;
import com.example.customerapp.Remote.RetrofitClient;
import com.example.customerapp.Service.IpService;
import com.example.customerapp.Utills.Utills;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {

    Activity mActivity;
    EditText firstName,lastName,email,address,streetAddress,mobile;
    Context mContext;
    ImageView profileImage;
    private ProgressBar imgProgress;
    RelativeLayout imageLayout;
    IpService mService;
    Customer customer;
    LinearLayout userProfileContent,profileContentLoading;
    RelativeLayout profileImageLayout;
    FloatingActionButton editImageBtn;
    private static final int GALLERY_REQUEST_CODE = 838;
    private Uri selectedImage;
    Button update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        mActivity = this;
        mContext = this;
        mService = new RetrofitClient().getClient(this).create(IpService.class);

        Toolbar orderDisplayToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(orderDisplayToolbar);
        setTitle("Profile");

        LinearLayout profileContent = findViewById(R.id.linLay3);

        AccountHeader headerResult = Utills.buildHeader(true, savedInstanceState, mActivity, mActivity);
        Drawer result = Utills.drawerFront1(orderDisplayToolbar, savedInstanceState, mActivity, mActivity);

        firstName = findViewById(R.id.firstName);
        lastName = findViewById(R.id.lastName);
        email = findViewById(R.id.email);
        mobile = findViewById(R.id.mobile);
        streetAddress = findViewById(R.id.streetAddress);
        address = findViewById(R.id.address);
        profileImage = findViewById(R.id.profileImage);
        imgProgress = findViewById(R.id.img_progress);
        imageLayout = findViewById(R.id.profileImageLayout);
        profileContentLoading = findViewById(R.id.profileContentLoading);
        userProfileContent = findViewById(R.id.profileContentLoading);
        profileImageLayout = findViewById(R.id.profileImageLayout);
        editImageBtn = findViewById(R.id.editImageBtn);
        update = findViewById(R.id.updateProfile);

        HashMap<String, String> stringHashMap = new HashMap<>();
        stringHashMap.put("user_id", Utills.getUserID(mContext));

        mService.getCustomerById(stringHashMap).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.code() == 200){
                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            customer = new Customer();

                            customer.setUserId(jsonArray.getJSONObject(i).getString("user_id"));
                            customer.setFirstName(jsonArray.getJSONObject(i).getString("first_name"));
                            customer.setLastName(jsonArray.getJSONObject(i).getString("last_name"));
                            customer.setFullName(jsonArray.getJSONObject(i).getString("first_name") + jsonArray.getJSONObject(i).getString("last_name"));
                            customer.setEmail(jsonArray.getJSONObject(i).getString("email"));

                            if (jsonArray.getJSONObject(i).getJSONArray("customerDetail").length() != 0) {
                                customer.setStreetAddress(jsonArray.getJSONObject(i).getJSONArray("customerDetail").getJSONObject(0).getString("streat_address"));
                                customer.setAddress(jsonArray.getJSONObject(i).getJSONArray("customerDetail").getJSONObject(0).getString("address"));
                                customer.setMobile(jsonArray.getJSONObject(i).getJSONArray("customerDetail").getJSONObject(0).getString("mobile"));
                                customer.setUserImage(jsonArray.getJSONObject(i).getJSONArray("customerDetail").getJSONObject(0).getString("user_image"));
                            }
                        }

                        userProfileContent.setVisibility(View.VISIBLE);
                        profileImageLayout.setVisibility(View.VISIBLE);
                        profileContentLoading.setVisibility(View.GONE);

                        firstName.setText(customer.getFirstName());
                        lastName.setText(customer.getLastName());
                        email.setText(customer.getEmail());
                        mobile.setText(customer.getMobile());
                        streetAddress.setText(customer.getStreetAddress());
                        address.setText(customer.getAddress());

                        if (!customer.getUserImage().equalsIgnoreCase("") && customer.getUserImage() != null && !customer.getUserImage().equalsIgnoreCase("null")) {
                            profileImage.setVisibility(View.GONE);

                            Picasso.get().load(customer.getUserImage())
                                    .into(profileImage, new com.squareup.picasso.Callback() {
                                        @Override
                                        public void onSuccess() {
                                            imgProgress.setVisibility(View.GONE);
                                            profileImage.setVisibility(View.VISIBLE);
                                        }

                                        @Override
                                        public void onError(Exception e) {
                                            profileImage.setVisibility(View.VISIBLE);
                                            profileImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.person));


                                        }

                                    });


                        } else {
                            profileImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.person));

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });

        editImageBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                pickFromGallery();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(firstName.getText().toString())) {
                    firstName.setError(getString(R.string.first_name_required));
                    firstName.requestFocus();
                } else if (TextUtils.isEmpty(lastName.getText().toString())) {
                    lastName.setError(getString(R.string.last_name_required));
                    lastName.requestFocus();
                } else if (TextUtils.isEmpty(email.getText().toString())) {
                    email.setError(getString(R.string.email_required));
                    email.requestFocus();
                } else if (TextUtils.isEmpty(mobile.getText().toString())) {
                    mobile.setError(getString(R.string.mobile_required));
                    mobile.requestFocus();
                }  else if (TextUtils.isEmpty(streetAddress.getText().toString())) {
                    streetAddress.setError(getString(R.string.street_address_required));
                    streetAddress.requestFocus();
                } else if (TextUtils.isEmpty(address.getText().toString())) {
                    address.setError(getString(R.string.address_required));
                    address.requestFocus();
                }else {
                    HashMap<String, String> stringHashMap = new HashMap<>();
                    stringHashMap.put("user_id", Utills.getUserID(mContext));
                    stringHashMap.put("first_name", firstName.getText().toString());
                    stringHashMap.put("last_name", lastName.getText().toString());
                    stringHashMap.put("email", email.getText().toString());
                    stringHashMap.put("streat_address", streetAddress.getText().toString());
                    stringHashMap.put("address", address.getText().toString());
                    stringHashMap.put("mobile", mobile.getText().toString());

                    mService.updateUser(stringHashMap).enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            if (response.code() == 200) {
                                if (selectedImage != null) {
                                    insertImage();
                                } else {
                                    Utills.dialogBox("USER PROFILE UPDATE", "Edit profile successfully", mContext);

                                }
                            } else {
                                Utills.dialogBox("USER PROFILE UPDATE", response.message(), mContext);
                            }

                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            Utills.dialogBox("USER PROFILE UPDATE", t.getMessage(), mContext);
                        }
                    });
                }
            }
        });

    }
    public void insertImage() {
        if (selectedImage != null) {

            File file = saveBitmapToFile(new File(getRealPathFromURI(selectedImage)));

            final RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("profile_image[]", file.getName(), requestBody);
            RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());

            RequestBody userId = createPartFromString(Utills.getUserID(mContext));

            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("user_id", userId);

            mService.insertProfileImage(map,fileToUpload,filename).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if(response.code() ==200){
                        Utills.dialogBox("USER PROFILE UPDATE","Edit profile successfully",mContext);

                    }else{
                        Utills.dialogBox("USER PROFILE UPDATE","Edit profile not updated",mContext);

                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Utills.dialogBox("USER PROFILE UPDATE",t.getMessage(),mContext);

                }
            });
        }
    }

    public File saveBitmapToFile(File file) {
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE = 75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }

    private RequestBody createPartFromString(String param) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), param);
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void pickFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(intent, GALLERY_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
            if (requestCode == GALLERY_REQUEST_CODE) {
                selectedImage = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                    profileImage.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }
}