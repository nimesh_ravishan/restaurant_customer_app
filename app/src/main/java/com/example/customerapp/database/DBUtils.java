package com.example.customerapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.example.customerapp.Model.Category;
import com.example.customerapp.Model.Product;
import com.example.customerapp.Model.Restaurant;
import com.example.customerapp.Model.SubCategory;

import java.util.ArrayList;

public class DBUtils {

    private static DbManager DbManager;
    private static final String TAG = "DBUtils";

    // insert data to category table
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void insertCategoriesToTable(ArrayList<Category> categoryList, Context mContext) {
        DbManager = new DbManager(mContext);

        try {
            SQLiteDatabase db = DbManager.getWritableDatabase();
            for (int i = 0; i < categoryList.size(); i++) {
                Category singleData = categoryList.get(i);

                ContentValues cv = new ContentValues();
                cv.put(DbTables.CATEGORY_ID, singleData.getCategoryId());
                cv.put(DbTables.CATEGORY_NAME, singleData.getCategoryName());
                cv.put(DbTables.CATEGORY_IMAGE, singleData.getCategoryImage());
                cv.put(DbTables.CATEGORY_AVAILABILITY, singleData.getCategoryAvailability());

                try {
                    long rowCount = db.insertOrThrow(DbTables.TABLE_NAME_DISPLAY_CATEGORIES, null, cv);
                    if (rowCount != -1) {
                        Log.e(TAG, "insertCategoriesToTable " + "category inserted");
                    } else {
                        long rowCountReplace = db.replaceOrThrow(DbTables.TABLE_NAME_DISPLAY_CATEGORIES, null, cv);
                        if (rowCountReplace != -1) {
                            Log.e(TAG, "insertCategoriesToTable " + "category replaced");
                        } else {
                            Log.e(TAG, "insertCategoriesToTable " + "category updated failed");
                        }
                        Log.e(TAG, "insertCategoriesToTable " + "category added failed");
                    }
                } catch (Exception e) {
                    Log.e(TAG, "insertCategoriesToTable " + e.toString());
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "insertCategoriesToTable " + e.toString());
        }
    }

    // insert data to category table
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void insertRestaurantToTable(Restaurant restaurant, Context mContext) {
        DbManager = new DbManager(mContext);

        try {
            SQLiteDatabase db = DbManager.getWritableDatabase();
            Restaurant singleData = restaurant;

            ContentValues cv = new ContentValues();
            cv.put(DbTables.RESTAURANT_ID, singleData.getRestaurantId());
            cv.put(DbTables.RESTAURANT_NAME, singleData.getRestaurantName());
            cv.put(DbTables.RESTAURANT_LAT_LONG1, singleData.getLatLong1());
            cv.put(DbTables.RESTAURANT_LAT_LONG2, singleData.getLatLong2());
            cv.put(DbTables.RESTAURANT_LAT_LONG3, singleData.getLatLong3());
            cv.put(DbTables.RESTAURANT_LAT_LONG4, singleData.getLatLong4());

            try {
                long rowCount = db.insertOrThrow(DbTables.TABLE_NAME_DISPLAY_RESTAURANT, null, cv);
                if (rowCount != -1) {
                    Log.e(TAG, "insertCategoriesToTable " + "category inserted");
                } else {
                    long rowCountReplace = db.replaceOrThrow(DbTables.TABLE_NAME_DISPLAY_RESTAURANT, null, cv);
                    if (rowCountReplace != -1) {
                        Log.e(TAG, "insertCategoriesToTable " + "category replaced");
                    } else {
                        Log.e(TAG, "insertCategoriesToTable " + "category updated failed");
                    }
                    Log.e(TAG, "insertCategoriesToTable " + "category added failed");
                }
            } catch (Exception e) {
                Log.e(TAG, "insertCategoriesToTable " + e.toString());
            }
        } catch (Exception e) {
            Log.e(TAG, "insertCategoriesToTable " + e.toString());
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static ArrayList<Category> getCategory(Context mContext) {
        DbManager = new DbManager(mContext);
        Cursor c;
        ArrayList<Category> categoryArrayList = new ArrayList<>();

        StringBuilder query = new StringBuilder("SELECT * FROM " + DbTables.TABLE_NAME_DISPLAY_CATEGORIES);

        try (SQLiteDatabase db = DbManager.getWritableDatabase()) {
            c = db.rawQuery(query.toString(), null);
            if (c.moveToFirst()) {
                do {
                    Category category = new Category();
                    category.setCategoryId(c.getString(c.getColumnIndex(DbTables.CATEGORY_ID)));
                    category.setCategoryName(c.getString(c.getColumnIndex(DbTables.CATEGORY_NAME)));
                    category.setCategoryImage(c.getString(c.getColumnIndex(DbTables.CATEGORY_IMAGE)));
                    category.setCategoryAvailability(c.getInt(c.getColumnIndex(DbTables.CATEGORY_AVAILABILITY)));

                    categoryArrayList.add(category);
                } while (c.moveToNext());
            }
            return categoryArrayList;
        } catch (Exception e) {
            Log.e(TAG, "getAllCategory " + e.toString());
        }
        return categoryArrayList;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static Restaurant getRestaurant(Context mContext) {
        DbManager = new DbManager(mContext);
        Cursor c;
        StringBuilder query = new StringBuilder("SELECT * FROM " + DbTables.TABLE_NAME_DISPLAY_RESTAURANT);
        Restaurant restaurant = new Restaurant();

        try (SQLiteDatabase db = DbManager.getWritableDatabase()) {
            c = db.rawQuery(query.toString(), null);
            if (c.moveToFirst()) {
                do {

                    restaurant.setRestaurantId(c.getString(c.getColumnIndex(DbTables.RESTAURANT_ID)));
                    restaurant.setRestaurantName(c.getString(c.getColumnIndex(DbTables.RESTAURANT_NAME)));
                    restaurant.setLatLong1(c.getString(c.getColumnIndex(DbTables.RESTAURANT_LAT_LONG1)));
                    restaurant.setLatLong2(c.getString(c.getColumnIndex(DbTables.RESTAURANT_LAT_LONG2)));
                    restaurant.setLatLong3(c.getString(c.getColumnIndex(DbTables.RESTAURANT_LAT_LONG3)));
                    restaurant.setLatLong4(c.getString(c.getColumnIndex(DbTables.RESTAURANT_LAT_LONG4)));

                } while (c.moveToNext());
            }
            return restaurant;
        } catch (Exception e) {
            Log.e(TAG, "getAllCategory " + e.toString());
        }
        return restaurant;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getCategoryIdByName(Context mContext, String categoryName) {
        DbManager = new DbManager(mContext);
        Cursor c;
        String categoryId = "";
        String query = "select * from " + DbTables.TABLE_NAME_DISPLAY_CATEGORIES + " where " + DbTables.CATEGORY_NAME + " = " + "\"" + categoryName + "\"" + "";

        try (SQLiteDatabase db = DbManager.getWritableDatabase()) {
            c = db.rawQuery(query.toString(), null);
            if (c.moveToFirst()) {
                do {
                    categoryId = c.getString(c.getColumnIndex(DbTables.CATEGORY_ID));
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getAllSubCategory " + e.toString());
        }
        return categoryId;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void deleteCategory(Context mContext) {

        DbManager = new DbManager(mContext);
        try (SQLiteDatabase db = DbManager.getWritableDatabase()) {

            long rowCount = db.delete(DbTables.TABLE_NAME_DISPLAY_CATEGORIES, null, null);
            if (rowCount != -1) {
                Log.e("category", "deleted.");
            } else {
                Log.i("category", "cleared");
            }

        } catch (Exception e) {
            Log.e("delete all from cat", e.getMessage());
        }

    }


    // insert data to category table
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void insertSubCategoriesToTable(ArrayList<SubCategory> subCategories, Context mContext) {
        DbManager = new DbManager(mContext);

        try {
            SQLiteDatabase db = DbManager.getWritableDatabase();
            for (int i = 0; i < subCategories.size(); i++) {
                SubCategory singleData = subCategories.get(i);

                ContentValues cv = new ContentValues();
                cv.put(DbTables.SUBCATEGORY_ID, singleData.getSubCategoryId());
                cv.put(DbTables.SUBCATEGORY_NAME, singleData.getSubCategoryName());
                cv.put(DbTables.SUBCATEGORY_IMAGE, singleData.getSubCategoryImage());
                cv.put(DbTables.SUBCATEGORY_CATEGORYID, singleData.getCategoryId());
                cv.put(DbTables.SUBCATEGORY_AVAILABILITY, singleData.getSubCategoryAvailability());

                try {
                    long rowCount = db.insertOrThrow(DbTables.TABLE_NAME_DISPLAY_SUBCATEGORIES, null, cv);
                    if (rowCount != -1) {
                        Log.e(TAG, "insertSubCategoriesToTable " + "SubCategory inserted");
                    } else {
                        long rowCountReplace = db.replaceOrThrow(DbTables.TABLE_NAME_DISPLAY_SUBCATEGORIES, null, cv);
                        if (rowCountReplace != -1) {
                            Log.e(TAG, "insertSubCategoriesToTable " + "SubCategory replaced");
                        } else {
                            Log.e(TAG, "insertSubCategoriesToTable " + "SubCategory updated failed");
                        }
                        Log.e(TAG, "insertSubCategoriesToTable " + "SubCategory added failed");
                    }
                } catch (Exception e) {
                    Log.e(TAG, "insertSubCategoriesToTable " + e.toString());
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "insertSubCategoriesToTable " + e.toString());
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static ArrayList<SubCategory> getSubcategoryBYCategoryId(Context mContext, String categoryId) {
        DbManager = new DbManager(mContext);
        Cursor c;
        ArrayList<SubCategory> subCategories = new ArrayList<>();

        String query = "select * from " + DbTables.TABLE_NAME_DISPLAY_SUBCATEGORIES + " where " + DbTables.SUBCATEGORY_CATEGORYID + " = " + "\"" + categoryId + "\"" + "";

        try (SQLiteDatabase db = DbManager.getWritableDatabase()) {
            c = db.rawQuery(query.toString(), null);
            if (c.moveToFirst()) {
                do {
                    SubCategory subCategory = new SubCategory();
                    subCategory.setSubCategoryId(c.getString(c.getColumnIndex(DbTables.SUBCATEGORY_ID)));
                    subCategory.setSubCategoryName(c.getString(c.getColumnIndex(DbTables.SUBCATEGORY_NAME)));
                    subCategory.setCategoryId(c.getString(c.getColumnIndex(DbTables.SUBCATEGORY_CATEGORYID)));
                    subCategory.setSubCategoryImage(c.getString(c.getColumnIndex(DbTables.SUBCATEGORY_IMAGE)));
                    subCategory.setSubCategoryAvailability(c.getInt(c.getColumnIndex(DbTables.SUBCATEGORY_AVAILABILITY)));
                    subCategories.add(subCategory);
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getAllSubCategory " + e.toString());
        }
        return subCategories;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getSubCategoryIdByName(Context mContext, String subCategoryName) {
        DbManager = new DbManager(mContext);
        Cursor c;
        String subCategoryId = "";
        String query = "select * from " + DbTables.TABLE_NAME_DISPLAY_SUBCATEGORIES + " where " + DbTables.SUBCATEGORY_NAME + " = " + "\"" + subCategoryName + "\"" + "";

        try (SQLiteDatabase db = DbManager.getWritableDatabase()) {
            c = db.rawQuery(query.toString(), null);
            if (c.moveToFirst()) {
                do {
                    subCategoryId = c.getString(c.getColumnIndex(DbTables.SUBCATEGORY_ID));
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getAllSubCategory " + e.toString());
        }
        return subCategoryId;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void deleteSubCategory(Context mContext) {

        DbManager = new DbManager(mContext);
        try (SQLiteDatabase db = DbManager.getWritableDatabase()) {

            long rowCount = db.delete(DbTables.TABLE_NAME_DISPLAY_SUBCATEGORIES, null, null);
            if (rowCount != -1) {
                Log.e("SubCategory", "deleted.");
            } else {
                Log.i("SubCategory", "cleared");
            }

        } catch (Exception e) {
            Log.e("delete all from sub", e.getMessage());
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static ArrayList<Product> getProducts(Context mContext) {
        DbManager = new DbManager(mContext);
        Cursor c;
        ArrayList<Product> productArrayList = new ArrayList<>();

        StringBuilder query = new StringBuilder("SELECT * FROM " + DbTables.TABLE_NAME_DISPLAY_SUBCATEGORIES);

        try (SQLiteDatabase db = DbManager.getWritableDatabase()) {
            c = db.rawQuery(query.toString(), null);
            if (c.moveToFirst()) {
                do {
                    Product product = new Product();
                    product.setProductId(c.getString(c.getColumnIndex(DbTables.PRODUCT_ID)));
                    product.setProductName(c.getString(c.getColumnIndex(DbTables.PRODUCT_NAME)));
                    product.setProductDescription(c.getString(c.getColumnIndex(DbTables.PRODUCT_DESCRIPTION)));
                    product.setProductPrice(c.getString(c.getColumnIndex(DbTables.PRODUCT_PRICE)));
                    product.setProductImage(c.getString(c.getColumnIndex(DbTables.PRODUCT_IMAGE)));
                    product.setProductCategory(c.getString(c.getColumnIndex(DbTables.PRODUCT_CATEGORYID)));
                    product.setProductSubCategory(c.getString(c.getColumnIndex(DbTables.PRODUCT_SUBCATEGORYID)));
                    product.setProductDiscountPercentage(c.getString(c.getColumnIndex(DbTables.PRODUCT_DISCOUNT)));
                    product.setPromotion(c.getString(c.getColumnIndex(DbTables.PRODUCT_PROMOTION)));
                    product.setRatings(c.getString(c.getColumnIndex(DbTables.PRODUCT_RATINGS)));
                    product.setProductAvailability(c.getInt(c.getColumnIndex(DbTables.PRODUCT_AVAILABILITY)));
                    productArrayList.add(product);

                } while (c.moveToNext());
            }
            return productArrayList;
        } catch (Exception e) {
            Log.e(TAG, "getAllCategory " + e.toString());
        }
        return productArrayList;
    }


    // insert data to category table
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void insertProductsToTable(ArrayList<Product> productArrayList, Context mContext) {
        DbManager = new DbManager(mContext);

        try {
            SQLiteDatabase db = DbManager.getWritableDatabase();
            for (int i = 0; i < productArrayList.size(); i++) {
                Product singleData = productArrayList.get(i);

                ContentValues cv = new ContentValues();
                cv.put(DbTables.PRODUCT_ID, singleData.getProductId());
                cv.put(DbTables.PRODUCT_NAME, singleData.getProductName());
                cv.put(DbTables.PRODUCT_DESCRIPTION, singleData.getProductDescription());
                cv.put(DbTables.PRODUCT_PRICE, singleData.getProductPrice());
                cv.put(DbTables.PRODUCT_IMAGE, singleData.getProductImage());
                cv.put(DbTables.PRODUCT_CATEGORYID, singleData.getProductCategory());
                cv.put(DbTables.PRODUCT_SUBCATEGORYID, singleData.getProductSubCategory());
                cv.put(DbTables.PRODUCT_DISCOUNT, singleData.getProductDiscountPercentage());
                cv.put(DbTables.PRODUCT_PROMOTION, singleData.getPromotion());
                cv.put(DbTables.PRODUCT_RATINGS, singleData.getRatings());
                cv.put(DbTables.PRODUCT_AVAILABILITY, singleData.getProductAvailability());

                try {
                    long rowCount = db.insertOrThrow(DbTables.TABLE_NAME_DISPLAY_PRODUCT, null, cv);
                    if (rowCount != -1) {
                        Log.e(TAG, "insertProductsToTable " + "Product inserted");
                    } else {
                        long rowCountReplace = db.replaceOrThrow(DbTables.TABLE_NAME_DISPLAY_PRODUCT, null, cv);
                        if (rowCountReplace != -1) {
                            Log.e(TAG, "insertProductsToTable " + "Product replaced");
                        } else {
                            Log.e(TAG, "insertProductsToTable " + "Product updated failed");
                        }
                        Log.e(TAG, "insertProductsToTable " + "Product added failed");
                    }
                } catch (Exception e) {
                    Log.e(TAG, "insertProductsToTable " + e.toString());
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "insertProductsToTable " + e.toString());
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static Product getProductByProductId(Context mContext, String productId) {
        DbManager = new DbManager(mContext);
        Cursor c;
        String query = "select * from " + DbTables.TABLE_NAME_DISPLAY_PRODUCT + " where " + DbTables.PRODUCT_ID + " = " + "\"" + productId + "\"" + "";

        Product product = new Product();
        try (SQLiteDatabase db = DbManager.getWritableDatabase()) {
            c = db.rawQuery(query.toString(), null);
            if (c.moveToFirst()) {
                do {
                    product.setProductId(c.getString(c.getColumnIndex(DbTables.PRODUCT_ID)));
                    product.setProductName(c.getString(c.getColumnIndex(DbTables.PRODUCT_NAME)));
                    product.setProductDescription(c.getString(c.getColumnIndex(DbTables.PRODUCT_DESCRIPTION)));
                    product.setProductPrice(c.getString(c.getColumnIndex(DbTables.PRODUCT_PRICE)));
                    product.setProductImage(c.getString(c.getColumnIndex(DbTables.PRODUCT_IMAGE)));
                    product.setProductCategory(c.getString(c.getColumnIndex(DbTables.PRODUCT_CATEGORYID)));
                    product.setProductSubCategory(c.getString(c.getColumnIndex(DbTables.PRODUCT_SUBCATEGORYID)));
                    product.setProductDiscountPercentage(c.getString(c.getColumnIndex(DbTables.PRODUCT_DISCOUNT)));
                    product.setPromotion(c.getString(c.getColumnIndex(DbTables.PRODUCT_PROMOTION)));
                    product.setRatings(c.getString(c.getColumnIndex(DbTables.PRODUCT_RATINGS)));
                    product.setProductAvailability(c.getInt(c.getColumnIndex(DbTables.PRODUCT_AVAILABILITY)));

                } while (c.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getAllSubCategory " + e.toString());
        }
        return product;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getProductNameByProductId(Context mContext, String productId) {
        DbManager = new DbManager(mContext);
        Cursor c;
        String productName = null;

        String query = "select * from " + DbTables.TABLE_NAME_DISPLAY_PRODUCT + " where " + DbTables.PRODUCT_ID + " = " + "\"" + productId + "\"" + "";

        try (SQLiteDatabase db = DbManager.getWritableDatabase()) {
            c = db.rawQuery(query.toString(), null);
            if (c.moveToFirst()) {
                do {
                    productName = (c.getString(c.getColumnIndex(DbTables.PRODUCT_NAME)));
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getAllSubCategory " + e.toString());
        }
        return productName;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getProductIdByProductName(Context mContext, String productName) {
        DbManager = new DbManager(mContext);
        Cursor c;
        String productId = null;

        String query = "select * from " + DbTables.TABLE_NAME_DISPLAY_PRODUCT + " where " + DbTables.PRODUCT_NAME + " = " + "\"" + productName + "\"" + "";

        try (SQLiteDatabase db = DbManager.getWritableDatabase()) {
            c = db.rawQuery(query.toString(), null);
            if (c.moveToFirst()) {
                do {
                    productId = (c.getString(c.getColumnIndex(DbTables.PRODUCT_ID)));
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getAllSubCategory " + e.toString());
        }
        return productId;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getProductPriceByProductId(Context mContext, String productId) {
        DbManager = new DbManager(mContext);
        Cursor c;
        String productPrice = null;

        String query = "select * from " + DbTables.TABLE_NAME_DISPLAY_PRODUCT + " where " + DbTables.PRODUCT_ID + " = " + "\"" + productId + "\"" + "";

        try (SQLiteDatabase db = DbManager.getWritableDatabase()) {
            c = db.rawQuery(query.toString(), null);
            if (c.moveToFirst()) {
                do {
                    productPrice = (c.getString(c.getColumnIndex(DbTables.PRODUCT_PRICE)));
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getAllSubCategory " + e.toString());
        }
        return productPrice;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getProductImageByProductId(Context mContext, String productId) {
        DbManager = new DbManager(mContext);
        Cursor c;
        String productImage = null;

        String query = "select * from " + DbTables.TABLE_NAME_DISPLAY_PRODUCT + " where " + DbTables.PRODUCT_ID + " = " + "\"" + productId + "\"" + "";

        try (SQLiteDatabase db = DbManager.getWritableDatabase()) {
            c = db.rawQuery(query.toString(), null);
            if (c.moveToFirst()) {
                do {
                    productImage = (c.getString(c.getColumnIndex(DbTables.PRODUCT_IMAGE)));
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getAllSubCategory " + e.toString());
        }
        return productImage;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static ArrayList<Product> getProductBYSubCategoryId(Context mContext, String subCategoryId) {
        DbManager = new DbManager(mContext);
        Cursor c;
        ArrayList<Product> productArrayList = new ArrayList<>();

        String query = "select * from " + DbTables.TABLE_NAME_DISPLAY_PRODUCT + " where " + DbTables.PRODUCT_SUBCATEGORYID + " = " + "\"" + subCategoryId + "\"" + "";

        try (SQLiteDatabase db = DbManager.getWritableDatabase()) {
            c = db.rawQuery(query.toString(), null);
            if (c.moveToFirst()) {
                do {
                    Product product = new Product();
                    product.setProductId(c.getString(c.getColumnIndex(DbTables.PRODUCT_ID)));
                    product.setProductName(c.getString(c.getColumnIndex(DbTables.PRODUCT_NAME)));
                    product.setProductDescription(c.getString(c.getColumnIndex(DbTables.PRODUCT_DESCRIPTION)));
                    product.setProductPrice(c.getString(c.getColumnIndex(DbTables.PRODUCT_PRICE)));
                    product.setProductImage(c.getString(c.getColumnIndex(DbTables.PRODUCT_IMAGE)));
                    product.setProductCategory(c.getString(c.getColumnIndex(DbTables.PRODUCT_CATEGORYID)));
                    product.setProductSubCategory(c.getString(c.getColumnIndex(DbTables.PRODUCT_SUBCATEGORYID)));
                    product.setProductDiscountPercentage(c.getString(c.getColumnIndex(DbTables.PRODUCT_DISCOUNT)));
                    product.setPromotion(c.getString(c.getColumnIndex(DbTables.PRODUCT_PROMOTION)));
                    product.setRatings(c.getString(c.getColumnIndex(DbTables.PRODUCT_RATINGS)));
                    product.setProductAvailability(c.getInt(c.getColumnIndex(DbTables.PRODUCT_AVAILABILITY)));
                    productArrayList.add(product);

                } while (c.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getAllSubCategory " + e.toString());
        }
        return productArrayList;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void deleteProduct(Context mContext) {

        DbManager = new DbManager(mContext);
        try (SQLiteDatabase db = DbManager.getWritableDatabase()) {

            long rowCount = db.delete(DbTables.TABLE_NAME_DISPLAY_PRODUCT, null, null);
            if (rowCount != -1) {
                Log.e("SubCategory", "deleted.");
            } else {
                Log.i("SubCategory", "cleared");
            }

        } catch (Exception e) {
            Log.e("delete all from sub", e.getMessage());
        }

    }


}
