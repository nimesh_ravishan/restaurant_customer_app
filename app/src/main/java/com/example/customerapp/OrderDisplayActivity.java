package com.example.customerapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Toast;

import com.example.customerapp.Adapter.OrderAdapter;
import com.example.customerapp.Adapter.OrderProductList;
import com.example.customerapp.Model.OrderProduct;
import com.example.customerapp.Model.Orders;
import com.example.customerapp.OnClick.OrderClick;
import com.example.customerapp.OnClick.ProductClick;
import com.example.customerapp.Remote.RetrofitClient;
import com.example.customerapp.Service.IpService;
import com.example.customerapp.Utills.Utills;
import com.google.gson.JsonObject;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDisplayActivity extends AppCompatActivity implements OrderClick,ProductClick{

    Context mContext;
    Activity mActivity;
    private static RecyclerView recyclerView;
    private static StaggeredGridLayoutManager staggeredGridLayoutManager;
    ArrayList<Orders> ordersArrayList;
    private static OrderAdapter orderAdapter;
    IpService mService;
    LinearLayout loading;
    OrderClick orderClick;
    AlertDialog dialogView, dialogViewRating;
    OrderProductList orderProductList;
    ProductClick productClick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_display);

        mActivity = OrderDisplayActivity.this;
        mContext = OrderDisplayActivity.this;
        mService = new RetrofitClient().getClient(this).create(IpService.class);
        orderClick = this;
        productClick = this;

        Toolbar orderDisplayToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(orderDisplayToolbar);
        setTitle("Orders");

        AccountHeader headerResult = Utills.buildHeader(true, savedInstanceState, mActivity, mActivity);
        Drawer result = Utills.drawerFront1(orderDisplayToolbar, savedInstanceState, mActivity, mActivity);
        loading = findViewById(R.id.loading);

        recyclerView = findViewById(R.id.product_grid_view);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        ordersArrayList = new ArrayList<>();

        recyclerView.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);

        if (!Utills.isNetworkNotAvailable(mContext)) {

            HashMap<String, String> stringHashMap = new HashMap<>();
            stringHashMap.put("user_id",Utills.getUserID(mContext) );

            mService.getOrdersByUserId(stringHashMap).enqueue(new Callback<JsonObject>() {

                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.code() == 200) {

                        recyclerView.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(String.valueOf(response.body()));
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                Orders orders = new Orders();
                                orders.setOrder_id(jsonArray.getJSONObject(i).getString("order_id"));
                                orders.setOrder_status(jsonArray.getJSONObject(i).getString("order_status"));
                                orders.setOrder_type(jsonArray.getJSONObject(i).getString("order_type"));
                                orders.setPayment_type(jsonArray.getJSONObject(i).getString("payment_type"));
                                orders.setProducts(jsonArray.getJSONObject(i).getString("products"));
                                orders.setTable(jsonArray.getJSONObject(i).getString("table"));
                                orders.setUser_id(jsonArray.getJSONObject(i).getString("user_id"));
                                orders.setPrice(jsonArray.getJSONObject(i).getString("price"));
                                orders.setDate(jsonArray.getJSONObject(i).getString("date_and_time"));

                                if (Utills.getUserID(mContext).equalsIgnoreCase(jsonArray.getJSONObject(i).getString("user_id"))) {
                                    ordersArrayList.add(orders);
                                }
                            }
                            recyclerView.setLayoutManager(null);
                            recyclerView.setLayoutManager(staggeredGridLayoutManager);
                            orderAdapter = new OrderAdapter(mContext, ordersArrayList, orderClick);
                            recyclerView.setAdapter(orderAdapter);
                            orderAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Utills.dialogBox(getString(R.string.app_name), getString(R.string.error_message), mContext);
                        Log.e("PROMOTION", "SOMETHING WENT WRONG!");
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Utills.dialogBox(getString(R.string.app_name), t.getMessage(), mContext);

                }
            });
        } else {
            Utills.dialogBox(getString(R.string.network_tittle), getString(R.string.network_message), mContext);

        }

    }

    @Override
    public void onOrderItemClick(int position, ArrayList<OrderProduct> orderProduct) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = getLayoutInflater();
        View convertView = inflater.inflate(R.layout.order_products, null);
        alertDialog.setView(convertView);
        dialogView = alertDialog.create();
        dialogView.show();

        RecyclerView productList = dialogView.findViewById(R.id.orderProductList);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        productList.setLayoutManager(layoutManager);

        orderProductList = new OrderProductList(mContext, orderProduct, productClick);
        productList.setAdapter(orderProductList);

    }

    @Override
    public void onProductItemClick(int position, String product) {
        if (!product.equalsIgnoreCase("")) {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
            LayoutInflater inflater = getLayoutInflater();
            View convertView = inflater.inflate(R.layout.rating_layout, null);
            alertDialog.setView(convertView);
            dialogViewRating = alertDialog.create();
            dialogViewRating.show();

            final RatingBar ratingBar = dialogViewRating.findViewById(R.id.ratingCount);
            final EditText ratingComment = dialogViewRating.findViewById(R.id.ratingComment);
            Button addRating = dialogViewRating.findViewById(R.id.addRating);



            addRating.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String rateComment = ratingComment.getText().toString();
                    float rating = ratingBar.getRating();

                    if ((ratingBar.getRating() == 0)) {
                        Toast.makeText(mContext, "Please add rating count", Toast.LENGTH_SHORT).show();

                    } else if (TextUtils.isEmpty(rateComment)) {
                        Toast.makeText(mContext, "Please add rating comment", Toast.LENGTH_SHORT).show();

                    } else {
                        final HashMap<String, String> stringHashMap = new HashMap<>();
                        stringHashMap.put("product_id", product);
                        stringHashMap.put("user_id", Utills.getUserID(mContext));
                        stringHashMap.put("rate_comment", ratingComment.getText().toString());
                        stringHashMap.put("rate_count", String.valueOf(rating));

                        mService.addRate(stringHashMap).enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                if (response.code() == 200) {
                                    dialogView.dismiss();
                                    dialogViewRating.dismiss();
                                    Utills.dialogBox("Rating", "Rating added successfully!", mContext);
                                }
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                dialogView.dismiss();
                                Utills.dialogBox("Rating", getResources().getString(R.string.error_message), mContext);

                            }
                        });

                    }


                }
            });

        }
    }
}