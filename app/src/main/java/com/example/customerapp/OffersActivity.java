package com.example.customerapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.example.customerapp.Adapter.PromotionAdapter;
import com.example.customerapp.Model.Promotion;
import com.example.customerapp.Remote.RetrofitClient;
import com.example.customerapp.Service.IpService;
import com.example.customerapp.Utills.Utills;
import com.google.gson.JsonObject;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffersActivity extends AppCompatActivity {
    Context mContext;
    Activity mActivity;
    IpService mService;
    ArrayList<Promotion> promotionArrayList;
    RecyclerView productRecycle;
    PromotionAdapter productAdapter;
    LinearLayout loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);
        Toolbar orderDisplayToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(orderDisplayToolbar);
        setTitle("Offers");

        mActivity = this;
        mContext = this;
        AccountHeader headerResult = Utills.buildHeader(true, savedInstanceState, mActivity, mActivity);
        Drawer result = Utills.drawerFront1(orderDisplayToolbar, savedInstanceState, mActivity, mActivity);
        productRecycle = findViewById(R.id.offers_grid_view);
        loading = findViewById(R.id.loading);

        productRecycle.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);

        mService = new RetrofitClient().getClient(this).create(IpService.class);

        if (!Utills.isNetworkNotAvailable(mContext)) {
            mService.getPromotion().enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.code() == 200) {
                        productRecycle.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            promotionArrayList = new ArrayList<>();

                            for (int x = 0; x < jsonArray.length(); x++) {
                                Promotion promotion = new Promotion();
                                promotion.setPromotionId(String.valueOf(jsonArray.getJSONObject(x).get("promotion_id")));
                                promotion.setPromoProductId(String.valueOf(jsonArray.getJSONObject(x).get("product_id")));
                                promotion.setOfferPercentage(String.valueOf(jsonArray.getJSONObject(x).get("offer_percentage")));
                                promotion.setPromotionDescription(String.valueOf(jsonArray.getJSONObject(x).get("promotion_description")));
                                promotion.setPromotionActivation(Integer.valueOf(String.valueOf(jsonArray.getJSONObject(x).get("activate_promotion"))));

                                if(String.valueOf(jsonArray.getJSONObject(x).get("activate_promotion")).equalsIgnoreCase("1"))
                                {
                                    promotionArrayList.add(promotion);
                                }
                            }

                            productRecycle.setLayoutManager(new GridLayoutManager(mContext, 3));

                            productAdapter = new PromotionAdapter(mContext, promotionArrayList);
                            productRecycle.setAdapter(productAdapter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Utills.dialogBox(getString(R.string.app_name), getString(R.string.error_message), mContext);
                        Log.e("PROMOTION", "SOMETHING WENT WRONG!");
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Utills.dialogBox(getString(R.string.app_name), t.getMessage(), mContext);

                }
            });

        }else{
            Utills.dialogBox(getString(R.string.network_tittle), getString(R.string.network_message), mContext);

        }


    }
}