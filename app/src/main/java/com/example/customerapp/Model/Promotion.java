package com.example.customerapp.Model;

public class Promotion {
    String promotionId;
    String promoProductId;
    String offerPercentage;
    String promotionDescription;
    int promotionActivation;

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public String getPromoProductId() {
        return promoProductId;
    }

    public void setPromoProductId(String promoProductId) {
        this.promoProductId = promoProductId;
    }

    public String getOfferPercentage() {
        return offerPercentage;
    }

    public void setOfferPercentage(String offerPercentage) {
        this.offerPercentage = offerPercentage;
    }

    public String getPromotionDescription() {
        return promotionDescription;
    }

    public void setPromotionDescription(String promotionDescription) {
        this.promotionDescription = promotionDescription;
    }

    public int getPromotionActivation() {
        return promotionActivation;
    }

    public void setPromotionActivation(int promotionActivation) {
        this.promotionActivation = promotionActivation;
    }
}
