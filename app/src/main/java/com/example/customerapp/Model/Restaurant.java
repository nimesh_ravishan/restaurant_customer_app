package com.example.customerapp.Model;

public class Restaurant {

    String restaurantId;
    String restaurantName;
    String latLong1;
    String latLong2;
    String latLong3;
    String latLong4;

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getLatLong1() {
        return latLong1;
    }

    public void setLatLong1(String latLong1) {
        this.latLong1 = latLong1;
    }

    public String getLatLong2() {
        return latLong2;
    }

    public void setLatLong2(String latLong2) {
        this.latLong2 = latLong2;
    }

    public String getLatLong3() {
        return latLong3;
    }

    public void setLatLong3(String latLong3) {
        this.latLong3 = latLong3;
    }

    public String getLatLong4() {
        return latLong4;
    }

    public void setLatLong4(String latLong4) {
        this.latLong4 = latLong4;
    }
}
