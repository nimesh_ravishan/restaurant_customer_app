package com.example.customerapp.Model;

public class Rating {
    String ratingId;
    String productId;
    String userId;
    String rateComment;
    String rateCount;
    String rateDateAndTime;
    String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRatingId() {
        return ratingId;
    }

    public void setRatingId(String ratingId) {
        this.ratingId = ratingId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRateComment() {
        return rateComment;
    }

    public void setRateComment(String rateComment) {
        this.rateComment = rateComment;
    }

    public String getRateCount() {
        return rateCount;
    }

    public void setRateCount(String rateCount) {
        this.rateCount = rateCount;
    }

    public String getRateDateAndTime() {
        return rateDateAndTime;
    }

    public void setRateDateAndTime(String rateDateAndTime) {
        this.rateDateAndTime = rateDateAndTime;
    }
}
