package com.example.customerapp.Model;

public class Category {

    String categoryId;
    String categoryName;
    String CategoryImage;
    int categoryAvailability;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryImage() {
        return CategoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        CategoryImage = categoryImage;
    }

    public int getCategoryAvailability() {
        return categoryAvailability;
    }

    public void setCategoryAvailability(int categoryAvailability) {
        this.categoryAvailability = categoryAvailability;
    }
}


