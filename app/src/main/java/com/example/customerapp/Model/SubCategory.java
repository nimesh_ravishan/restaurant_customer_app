package com.example.customerapp.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class SubCategory implements Parcelable {

    String subCategoryId;
    String subCategoryName;
    String categoryId;
    String subCategoryImage;

    public SubCategory(Parcel in) {
        subCategoryId = in.readString();
        subCategoryName = in.readString();
        categoryId = in.readString();
        subCategoryImage = in.readString();
        subCategoryAvailability = in.readInt();
    }

    public static final Creator<SubCategory> CREATOR = new Creator<SubCategory>() {
        @Override
        public SubCategory createFromParcel(Parcel in) {
            return new SubCategory(in);
        }

        @Override
        public SubCategory[] newArray(int size) {
            return new SubCategory[size];
        }
    };

    public SubCategory() {

    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    int subCategoryAvailability;

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getSubCategoryImage() {
        return subCategoryImage;
    }

    public void setSubCategoryImage(String subCategoryImage) {
        this.subCategoryImage = subCategoryImage;
    }

    public int getSubCategoryAvailability() {
        return subCategoryAvailability;
    }

    public void setSubCategoryAvailability(int subCategoryAvailability) {
        this.subCategoryAvailability = subCategoryAvailability;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(subCategoryId);
        dest.writeString(subCategoryName);
        dest.writeString(categoryId);
        dest.writeString(subCategoryImage);
        dest.writeInt(subCategoryAvailability);

    }
}
