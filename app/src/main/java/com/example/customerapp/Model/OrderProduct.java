package com.example.customerapp.Model;

public class OrderProduct {

    String order_product_id;
    String product_id;
    String quantity;
    String price;
    String offer;
    String comment;
    String order_id;

    public String getOrderProductId() {
        return order_product_id;
    }

    public void setOrderProductId(String orderProductId) {
        this.order_product_id = orderProductId;
    }

    public String getProductId() {
        return product_id;
    }

    public void setProductId(String productId) {
        this.product_id = productId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getOrderId() {
        return order_id;
    }

    public void setOrderId(String orderId) {
        this.order_id = orderId;
    }
}
