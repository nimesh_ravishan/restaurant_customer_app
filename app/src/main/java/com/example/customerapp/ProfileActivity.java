package com.example.customerapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.customerapp.Model.Customer;
import com.example.customerapp.Remote.RetrofitClient;
import com.example.customerapp.Service.IpService;
import com.example.customerapp.Utills.Utills;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    Activity mActivity;
    TextView fullName,email,address,mobile;
    Context mContext;
    ImageView profileImage,settings;
    private ProgressBar imgProgress;
    RelativeLayout imageLayout;
    IpService mService;
    Customer customer;
    LinearLayout userProfileContent,profileContentLoading;
    RelativeLayout profileImageLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mActivity = this;
        mContext = this;
        mService = new RetrofitClient().getClient(this).create(IpService.class);

        Toolbar orderDisplayToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(orderDisplayToolbar);
        setTitle("Profile");

        LinearLayout profileContent = findViewById(R.id.linLay3);

        AccountHeader headerResult = Utills.buildHeader(true, savedInstanceState, mActivity, mActivity);
        Drawer result = Utills.drawerFront1(orderDisplayToolbar, savedInstanceState, mActivity, mActivity);

        fullName = findViewById(R.id.tvFullName);
        email = findViewById(R.id.tvEmail);
        address = findViewById(R.id.address);
        mobile = findViewById(R.id.tvmobile);
        profileImage = findViewById(R.id.profileImage);
        settings = findViewById(R.id.updateProfileBtn);
        imgProgress = findViewById(R.id.img_progress);
        imageLayout = findViewById(R.id.profileImageLayout);
        profileContentLoading = findViewById(R.id.profileContentLoading);
        userProfileContent = findViewById(R.id.profileContentLoading);
        profileImageLayout = findViewById(R.id.profileImageLayout);

        HashMap<String, String> stringHashMap = new HashMap<>();
        stringHashMap.put("user_id", Utills.getUserID(mContext));

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this,EditProfileActivity.class);
                startActivity(intent);
            }
        });


        mService.getCustomerById(stringHashMap).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.code() == 200){
                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            customer = new Customer();

                            customer.setUserId(jsonArray.getJSONObject(i).getString("user_id"));
                            customer.setFirstName(jsonArray.getJSONObject(i).getString("first_name"));
                            customer.setLastName(jsonArray.getJSONObject(i).getString("last_name"));
                            customer.setFullName(jsonArray.getJSONObject(i).getString("first_name") + jsonArray.getJSONObject(i).getString("last_name"));
                            customer.setEmail(jsonArray.getJSONObject(i).getString("email"));

                            if (jsonArray.getJSONObject(i).getJSONArray("customerDetail").length() != 0) {
                                customer.setStreetAddress(jsonArray.getJSONObject(i).getJSONArray("customerDetail").getJSONObject(0).getString("streat_address"));
                                customer.setAddress(jsonArray.getJSONObject(i).getJSONArray("customerDetail").getJSONObject(0).getString("address"));
                                customer.setMobile(jsonArray.getJSONObject(i).getJSONArray("customerDetail").getJSONObject(0).getString("mobile"));
                                customer.setUserImage(jsonArray.getJSONObject(i).getJSONArray("customerDetail").getJSONObject(0).getString("user_image"));
                            }
                        }

                        userProfileContent.setVisibility(View.VISIBLE);
                        profileImageLayout.setVisibility(View.VISIBLE);
                        profileContentLoading.setVisibility(View.GONE);

                        fullName.setText(customer.getFullName());
                        email.setText(customer.getEmail());
                        mobile.setText(customer.getMobile());
                        address.setText(customer.getStreetAddress() + " "+ customer.getAddress());

                        if (!customer.getUserImage().equalsIgnoreCase("") && customer.getUserImage() != null && !customer.getUserImage().equalsIgnoreCase("null")) {
                            profileImage.setVisibility(View.GONE);

                            Picasso.get().load(customer.getUserImage())
                                    .into(profileImage, new com.squareup.picasso.Callback() {
                                        @Override
                                        public void onSuccess() {
                                            imgProgress.setVisibility(View.GONE);
                                            profileImage.setVisibility(View.VISIBLE);
                                        }

                                        @Override
                                        public void onError(Exception e) {
                                            profileImage.setVisibility(View.VISIBLE);
                                            profileImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.person));


                                        }

                                    });


                        } else {
                            profileImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.person));

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });

    }
}