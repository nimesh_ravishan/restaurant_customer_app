package com.example.customerapp.OnClick;

public interface ProductClick {
    void onProductItemClick(int position, String product);
}
