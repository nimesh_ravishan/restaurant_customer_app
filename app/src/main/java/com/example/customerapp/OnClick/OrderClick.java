package com.example.customerapp.OnClick;

import com.example.customerapp.Model.OrderProduct;

import java.util.ArrayList;

public interface OrderClick {
    void onOrderItemClick(int position, ArrayList<OrderProduct> orderProduct);

}
