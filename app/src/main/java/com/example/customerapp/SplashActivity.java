package com.example.customerapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.example.customerapp.Model.Category;
import com.example.customerapp.Model.Product;
import com.example.customerapp.Model.Promotion;
import com.example.customerapp.Model.Restaurant;
import com.example.customerapp.Model.SubCategory;
import com.example.customerapp.Remote.RetrofitClient;
import com.example.customerapp.Service.IpService;
import com.example.customerapp.Utills.Utills;
import com.example.customerapp.database.DBUtils;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    public static final String TAG = "SPLASH SCREEN";
    IpService mService;
    Context mContext;
    ArrayList<SubCategory> subCategoryArrayList;
    ArrayList<Product> productArrayList;
    private final int REQUEST_LOCATION_PERMISSION = 1;
    Activity mActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mContext = SplashActivity.this;
        mService = new RetrofitClient().getClient(this).create(IpService.class);



        DBUtils.deleteCategory(mContext);
        DBUtils.deleteSubCategory(mContext);
        DBUtils.deleteProduct(mContext);
        insertCategory();
        getRestaurant();
        insertProduct();



    }
    private void requestStoragePermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent serviceIntent = new Intent(SplashActivity.this, LocationService.class);
                            serviceIntent.putExtra("KEY1", "service is started");
                            SplashActivity.this.startService(serviceIntent);

                            if (!Utills.getUserIsLogged(mContext)) {
                                Intent login = new Intent(SplashActivity.this, LoginActivity.class);
                                startActivity(login);
                                finish();
                            } else {
                                Intent dashboard = new Intent(SplashActivity.this, HomePageActivity.class);
                                startActivity(dashboard);
                                finish();
                            }
                        }else{
                            if (!Utills.getUserIsLogged(mContext)) {
                                Intent login = new Intent(SplashActivity.this, LoginActivity.class);
                                startActivity(login);
                                finish();
                            } else{
                                Intent dashboard = new Intent(SplashActivity.this, HomePageActivity.class);
                                startActivity(dashboard);
                                finish();
                            }
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }






    public void insertProduct() {
        if (!Utills.isNetworkNotAvailable(mContext)) {
            mService.getProducts().enqueue(new Callback<JsonObject>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.code() == 200) {
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            productArrayList = new ArrayList<>();

                            for (int x = 0; x < jsonArray.length(); x++) {
                                Product product = new Product();
                                product.setProductId(String.valueOf(jsonArray.getJSONObject(x).get("product_id")));
                                product.setProductName(String.valueOf(jsonArray.getJSONObject(x).get("product_name")));
                                product.setProductDescription(String.valueOf(jsonArray.getJSONObject(x).get("product_description")));
                                product.setProductPrice(String.valueOf(jsonArray.getJSONObject(x).get("product_price")));
                                product.setProductImage(String.valueOf(jsonArray.getJSONObject(x).get("product_image")));
                                product.setProductCategory(String.valueOf(jsonArray.getJSONObject(x).get("product_category")));
                                product.setProductSubCategory(String.valueOf(jsonArray.getJSONObject(x).get("product_sub_category")));
                                product.setProductDiscountPercentage(String.valueOf(jsonArray.getJSONObject(x).get("product_discount_percentage")));
                                product.setPromotion(String.valueOf(jsonArray.getJSONObject(x).get("promotion")));
                                product.setProductAvailability(Integer.valueOf(String.valueOf(jsonArray.getJSONObject(x).get("product_availability"))));
                                productArrayList.add(product);
                            }
                            DBUtils.insertProductsToTable(productArrayList, mContext);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "SOMETHING WENT WRONG!");
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
        } else {
            Utills.dialogBox(getString(R.string.network_tittle), getString(R.string.network_message), mContext);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void insertCategory() {
        if (!Utills.isNetworkNotAvailable(mContext)) {
            mService.getCategory().enqueue(new Callback<JsonObject>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.code() == 200) {
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            ArrayList<Category> categoryArrayList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                subCategoryArrayList = new ArrayList<>();
                                JSONObject categories = jsonArray.getJSONObject(i);

                                Category category = new Category();
                                category.setCategoryId(categories.getString("category_id"));
                                category.setCategoryName(categories.getString("category_name"));
                                category.setCategoryImage(categories.getString("category_image"));
                                category.setCategoryAvailability(categories.getInt("category_availability"));

                                JSONArray subCategories = categories.getJSONArray("subCategories");
                                if (subCategories.length() != 0) {
                                    for (int x = 0; x < subCategories.length(); x++) {
                                        SubCategory subCategory = new SubCategory();
                                        subCategory.setSubCategoryId(subCategories.getJSONObject(x).getString("sub_category_id"));
                                        subCategory.setCategoryId(subCategories.getJSONObject(x).getString("category_id"));
                                        subCategory.setSubCategoryName(subCategories.getJSONObject(x).getString("sub_category_name"));
                                        subCategory.setSubCategoryImage(subCategories.getJSONObject(x).getString("sub_category_image"));
                                        subCategory.setSubCategoryAvailability(subCategories.getJSONObject(x).getInt("sub_category_availability"));
                                        subCategoryArrayList.add(subCategory);
                                    }
                                    DBUtils.insertSubCategoriesToTable(subCategoryArrayList, mContext);
                                }
                                categoryArrayList.add(category);
                            }
                            DBUtils.insertCategoriesToTable(categoryArrayList, mContext);
                            requestStoragePermission();

//                            if (!Utills.getUserIsLogged(mContext)) {
//                                Intent login = new Intent(SplashActivity.this, LoginActivity.class);
//                                startActivity(login);
//                                finish();
//                            } else {
//                                Intent dashboard = new Intent(SplashActivity.this, HomePageActivity.class);
//                                startActivity(dashboard);
//                                finish();
//                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "SOMETHING WENT WRONG!");
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
        } else {
            Utills.dialogBox(getString(R.string.network_tittle), getString(R.string.network_message), mContext);
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void getRestaurant() {
        if (!Utills.isNetworkNotAvailable(mContext)) {
            mService.getRestaurant().enqueue(new Callback<JsonObject>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.code() == 200) {
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            Restaurant restaurants = new Restaurant();

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject restaurant = jsonArray.getJSONObject(i);

                                restaurants.setRestaurantId(restaurant.getString("restaurant_id"));
                                restaurants.setRestaurantName(restaurant.getString("restaurant_name"));
                                restaurants.setLatLong1(restaurant.getString("lat_long_1"));
                                restaurants.setLatLong2(restaurant.getString("lat_long_2"));
                                restaurants.setLatLong3(restaurant.getString("lat_long_3"));
                                restaurants.setLatLong4(restaurant.getString("lat_long_4"));

                            }
                            DBUtils.insertRestaurantToTable(restaurants, mContext);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "SOMETHING WENT WRONG!");
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
        } else {
            Utills.dialogBox(getString(R.string.network_tittle), getString(R.string.network_message), mContext);
        }
    }
}