package com.example.customerapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.customerapp.Model.Product;
import com.example.customerapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MostBuyProductdapter extends BaseAdapter {

    Context context;
    ArrayList<Product> products;

    public MostBuyProductdapter(Context context, ArrayList<Product> products) {
        this.context = context;
        this.products = products;
    }



    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final View gridView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            gridView = inflater.inflate(R.layout.most_buy_product_grid, null);



//            gridView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    onItemClickListener.onItemClick(i,categoryId);
//                }
//            });


            TextView productName = gridView.findViewById(R.id.product_name);
            final ImageView imageView = gridView.findViewById(R.id.product_item_image);
            final ProgressBar imgProgress = gridView.findViewById(R.id.img_progress);


            productName.setText(products.get(i).getProductName());

            if (products.get(i).getProductImage() == null || products.get(i).getProductImage().equalsIgnoreCase("") || products.get(i).getProductImage().equalsIgnoreCase("null")) {
                imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.logo));
            } else {
                imageView.setVisibility(View.GONE);
                Picasso.get().load(products.get(i).getProductImage())
                        .into(imageView, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                imgProgress.setVisibility(View.GONE);
                                imageView.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError(Exception e) {
                                imageView.setVisibility(View.VISIBLE);
                            }
                        });
            }


        } else {
            gridView = view;
        }


        return gridView;
    }
}
