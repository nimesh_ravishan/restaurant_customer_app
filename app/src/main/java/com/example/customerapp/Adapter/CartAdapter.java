package com.example.customerapp.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.example.customerapp.Model.CartObject;
import com.example.customerapp.OnClick.CartProductRemove;
import com.example.customerapp.R;

import java.util.ArrayList;


public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {
    private final Context mContext;
    private ArrayList<CartObject> cartList;
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();
    private int lastPosition = 0;
    private final LayoutInflater inflater;
    CartProductRemove cartProductRemove;


    public CartAdapter(Context context, ArrayList<CartObject> cartList, CartProductRemove cartProductRemove) {
        this.cartList = cartList;
        this.mContext = context;
        this.cartProductRemove = cartProductRemove;
        inflater = LayoutInflater.from(context);
        binderHelper.setOpenOnlyOne(true);
    }

    @Override
    public CartAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.cart_list_item, parent, false);
        return new CartAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CartAdapter.MyViewHolder holder, int position) {
        if (cartList != null && 0 <= position && position < cartList.size()) {
            final CartObject data = cartList.get(position);

            binderHelper.bind(holder.swipeLayout, data.getCartProductId());

            holder.bind(data, position);
            setAnimation(holder.itemView, position);
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.right_to_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        if (cartList == null)
            return 0;
        return cartList.size();
    }


    public void saveStates(Bundle outState) {
        binderHelper.saveStates(outState);
    }

    public void restoreStates(Bundle inState) {
        binderHelper.restoreStates(inState);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        final private SwipeRevealLayout swipeLayout;
        final private View frontLayout,actionLayout;
        private TextView itemName,itemCount,itemPrices,userComment;

        private MyViewHolder(View itemView) {
            super(itemView);
            swipeLayout = itemView.findViewById(R.id.swipe_layout);
            frontLayout = itemView.findViewById(R.id.front_layout);
            actionLayout = itemView.findViewById(R.id.delete_layout);
            itemName = itemView.findViewById(R.id.item_name);
            itemCount = itemView.findViewById(R.id.item_count);
            itemPrices = itemView.findViewById(R.id.price);
            userComment = itemView.findViewById(R.id.user_comments_text_view);

        }

        private void bind(CartObject data, final int position) {
            final CartObject cartObject = data;
            swipeLayout.setTag(cartObject.getCartProductName());

            frontLayout.setTag(position);

            actionLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cartProductRemove.onProductRemove(position);
                            cartList.remove(cartList.get(position));
                            notifyItemRemoved(position);
                            notifyDataSetChanged();

                        }
                    });

            itemName.setText(cartObject.getCartProductQuantity() + " x " + cartObject.getCartProductName());

            itemCount.setText(String.valueOf(cartObject.getCartProductQuantity()));

            itemPrices.setText(String.valueOf(cartObject.getCartProductQuantity() * cartObject.getCartProductPrice()));

            userComment.setVisibility(View.GONE);

            if (swipeLayout.isOpened())
                swipeLayout.close(true);

        }
    }

}
