package com.example.customerapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.customerapp.Model.Promotion;
import com.example.customerapp.OnClick.ProductClick;
import com.example.customerapp.R;
import com.example.customerapp.database.DBUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PromotionAdapter extends RecyclerView.Adapter<PromotionAdapter.MyViewHolder> {


    private final LayoutInflater inflater;
    Context context;
    ArrayList<Promotion> productArrayList;
    ProductClick productClick;

    public PromotionAdapter(Context context, ArrayList<Promotion> productArrayList) {
        this.context = context;
        this.productArrayList = productArrayList;
        inflater = LayoutInflater.from(context);

    }


    @NonNull
    @Override
    public PromotionAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.promotion_list_item, parent, false);
        return new PromotionAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final Promotion promotion = productArrayList.get(position);

        if (!promotion.getPromoProductId().equalsIgnoreCase("") && promotion.getPromoProductId() != null && !promotion.getPromoProductId().equalsIgnoreCase("null")) {
            holder.proName.setText(DBUtils.getProductNameByProductId(context,promotion.getPromoProductId()));

            holder.promotion.setVisibility(View.VISIBLE);
            holder.promotion.setText(promotion.getOfferPercentage()+ "%");
            holder.proPrice.setText(DBUtils.getProductPriceByProductId(context,promotion.getPromoProductId()));


            if (!promotion.getPromoProductId().equalsIgnoreCase("") && promotion.getPromoProductId() != null && !promotion.getPromoProductId().equalsIgnoreCase("null")) {
                holder.proImage.setVisibility(View.GONE);

                Picasso.get().load(DBUtils.getProductImageByProductId(context,promotion.getPromoProductId()))
                        .into(holder.proImage, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                holder.imgProgress.setVisibility(View.GONE);
                                holder.proImage.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError(Exception e) {
                                holder.proImage.setVisibility(View.VISIBLE);

                            }

                        });


            } else {
                holder.proImage.setImageDrawable(context.getResources().getDrawable(R.drawable.logo));

            }
        }
    }


    @Override
    public int getItemCount() {
        return productArrayList.size();
    }

    protected class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView proImage;
        TextView proName, proPrice, promotion;
        ProgressBar imgProgress;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            proImage = itemView.findViewById(R.id.proImage);
            promotion = itemView.findViewById(R.id.promotion);
            proName = itemView.findViewById(R.id.proName);
            proPrice = itemView.findViewById(R.id.proPrice);
            imgProgress = itemView.findViewById(R.id.img_progress);

        }
    }
}
