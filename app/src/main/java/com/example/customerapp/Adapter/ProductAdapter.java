package com.example.customerapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.customerapp.Model.Product;
import com.example.customerapp.OnClick.ProductClick;
import com.example.customerapp.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {


    private final LayoutInflater inflater;
    Context context;
    ArrayList<Product> productArrayList;
    ProductClick productClick;

    public ProductAdapter(Context context, ArrayList<Product> productArrayList, ProductClick productClick) {
        this.context = context;
        this.productArrayList = productArrayList;
        this.productClick = productClick;
        inflater = LayoutInflater.from(context);

    }


    @NonNull
    @Override
    public ProductAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.product_list_item, parent, false);
        return new ProductAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final Product product = productArrayList.get(position);

        if (!product.getProductName().equalsIgnoreCase("") && product.getProductName() != null && !product.getProductName().equalsIgnoreCase("null")) {
            holder.proName.setText(product.getProductName());
        }
        if (!product.getProductPrice().equalsIgnoreCase("") && product.getProductPrice() != null && !product.getProductPrice().equalsIgnoreCase("null")) {
            holder.proPrice.setText(product.getProductPrice());
        }
        if (!product.getProductImage().equalsIgnoreCase("") && product.getProductImage() != null && !product.getProductImage().equalsIgnoreCase("null")) {
            holder.proImage.setVisibility(View.GONE);

            Picasso.get().load(product.getProductImage())
                    .into(holder.proImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            holder.imgProgress.setVisibility(View.GONE);
                            holder.proImage.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onError(Exception e) {
                            holder.proImage.setVisibility(View.VISIBLE);

                        }

                    });


        } else {
            holder.proImage.setImageDrawable(context.getResources().getDrawable(R.drawable.logo));

        }

        if (!product.getPromotion().equalsIgnoreCase("[]")) {
            try {
                JSONArray jsonArray = new JSONArray(product.getPromotion());
                for (int i = 0; i < jsonArray.length(); i++) {
                    int promotionStatus = Integer.parseInt(jsonArray.getJSONObject(i).getString("activate_promotion"));

                    if (promotionStatus != 0) {
                        holder.promotion.setVisibility(View.VISIBLE);
                        holder.promotion.setText(jsonArray.getJSONObject(i).getString("offer_percentage") + "%");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productClick.onProductItemClick(position, product.getProductId());
            }
        });

    }


    @Override
    public int getItemCount() {
        return productArrayList.size();
    }

    protected class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView proImage;
        TextView proName, proPrice, promotion;
        ProgressBar imgProgress;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            proImage = itemView.findViewById(R.id.proImage);
            promotion = itemView.findViewById(R.id.promotion);
            proName = itemView.findViewById(R.id.proName);
            proPrice = itemView.findViewById(R.id.proPrice);
            imgProgress = itemView.findViewById(R.id.img_progress);

        }
    }
}
