package com.example.customerapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.customerapp.Model.Rating;
import com.example.customerapp.R;

import java.util.ArrayList;

public class RatingListAdapter extends RecyclerView.Adapter<RatingListAdapter.MyViewHolder> {


    private final LayoutInflater inflater;
    ArrayList<Rating> ratings;
    Context context;


    public RatingListAdapter(Context context, ArrayList<Rating> ratings ) {
        this.context = context;
        this.ratings = ratings;
        inflater = LayoutInflater.from(context);

    }

    @NonNull
    @Override
    public RatingListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.rating_list_view, parent, false);
        return new RatingListAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RatingListAdapter.MyViewHolder holder, int position) {
        holder.userName.setText(ratings.get(position).getUserName());
        holder.ratingCount.setText(ratings.get(position).getRateCount().toString());
        holder.rating_comment.setText(ratings.get(position).getRateComment());
        holder.rateDate.setText(ratings.get(position).getRateDateAndTime());
        holder.ratingBar.setRating(Float.parseFloat(ratings.get(position).getRateCount()));
    }

    @Override
    public int getItemCount() {
        return ratings.size();
    }


    protected class MyViewHolder extends RecyclerView.ViewHolder {


        TextView userName,ratingCount,rating_comment,rateDate;
        RatingBar ratingBar;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.username);
            ratingCount = itemView.findViewById(R.id.rating_count);
            rating_comment = itemView.findViewById(R.id.rating_comment);
            rateDate = itemView.findViewById(R.id.rateDate);
            ratingBar = itemView.findViewById(R.id.rating_star);

        }
    }

}
