package com.example.customerapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.customerapp.Model.OrderProduct;
import com.example.customerapp.OnClick.ProductClick;
import com.example.customerapp.R;
import com.example.customerapp.database.DBUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class OrderProductList extends RecyclerView.Adapter<OrderProductList.MyViewHolder> {

    private final LayoutInflater inflater;
    Context mContext;
    ArrayList<OrderProduct> orderProducts;
    private ViewGroup parent;
    ProductClick productClick;

    public OrderProductList(Context mContext, ArrayList<OrderProduct> orderProducts, ProductClick productClick) {
        this.mContext = mContext;
        this.orderProducts = orderProducts;
        inflater = LayoutInflater.from(mContext);
        this.productClick=productClick;

    }

    @NonNull
    @Override
    public OrderProductList.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.product_item_list_layout, parent, false);
        return new OrderProductList.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderProductList.MyViewHolder holder, final int position) {

        String quantity = String.valueOf(orderProducts.get(position).getQuantity());
        String productName = DBUtils.getProductNameByProductId(mContext,orderProducts.get(position).getProductId());
        String productImage = DBUtils.getProductImageByProductId(mContext,orderProducts.get(position).getProductId());

        Picasso.get().load(productImage)
                .into(holder.imgProduct, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        holder.imgProgress.setVisibility(View.GONE);
                        holder.imgProduct.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError(Exception e) {
                        holder.imgProduct.setVisibility(View.VISIBLE);

                    }

                });

        holder.textProductName.setText(productName);
        holder.textProductQuantity.setText("QTY : " + quantity);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productClick.onProductItemClick(position, orderProducts.get(position).getProductId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return orderProducts.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textProductQuantity, textProductName;
        ImageView imgProduct;
        ProgressBar imgProgress;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
             textProductQuantity = itemView.findViewById(R.id.list_product_qty);
             textProductName = itemView.findViewById(R.id.list_product_name);
             imgProduct = itemView.findViewById(R.id.list_product_image);
             imgProgress = itemView.findViewById(R.id.img_progress);
        }
    }
}
