package com.example.customerapp.Remote;

import android.content.Context;

import com.example.customerapp.Utills.Utills;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    static String BASE_URL = "http://192.168.1.5/resturantApi/";
//    static String BASE_URL = "http://192.168.8.101/resturantApi/";

//    static String BASE_URL = "http://192.168.43.18/resturantApi/";
//    static String BASE_URL = "http://192.168.8.124/resturantApi/";
    private static Retrofit retrofit = null;
    private static String token;

    private static Context mContext;


    public static Retrofit getClient(final Context context) {
        if (retrofit == null) {

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
            okHttpClientBuilder
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .addInterceptor(new Interceptor() {
                        @Override
                        public okhttp3.Response intercept(Chain chain) throws IOException {
                            Request request = chain.request();
                            Request.Builder newRequest = request.newBuilder();
                            newRequest.header("Authorization", "Bearer " + Utills.getToken(context));
                            newRequest.header("Accept", "application/json");
                            newRequest.header("Content-Type", "application/json");
                            return chain.proceed(newRequest.build());
                        }
                    });

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClientBuilder.build())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

}
