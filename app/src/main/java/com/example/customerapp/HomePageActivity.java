package com.example.customerapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.example.customerapp.Adapter.MostBuyProductdapter;
import com.example.customerapp.Model.Product;
import com.example.customerapp.Remote.RetrofitClient;
import com.example.customerapp.Service.IpService;
import com.example.customerapp.Utills.Utills;
import com.google.gson.JsonObject;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePageActivity extends AppCompatActivity {
    Context mContext;
    Activity mActivity;
    ArrayList<Product> mostBuyProducts;
    MostBuyProductdapter mostBuyProductdapter;
    IpService mService;
    GridView mostBuyProductList;
    LinearLayout loading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        Toolbar orderDisplayToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(orderDisplayToolbar);
        setTitle("Home Page");

        mActivity = this;
        mContext = this;

        AccountHeader headerResult = Utills.buildHeader(true, savedInstanceState, mActivity, mActivity);
        Drawer result = Utills.drawerFront1(orderDisplayToolbar, savedInstanceState, mActivity, mActivity);

        mService = new RetrofitClient().getClient(this).create(IpService.class);
        mostBuyProductList =  findViewById(R.id.gridView);
        loading =  findViewById(R.id.loading);

        getMostBuyProducts();
    }

    public void getMostBuyProducts() {
        if (!Utills.isNetworkNotAvailable(mContext)) {
            mService.getMostBuyProducts().enqueue(new Callback<JsonObject>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.code() == 200) {
                        try {
                            JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            mostBuyProducts = new ArrayList<>();
                            for (int x = 0; x < jsonArray.length(); x++) {
                                Product product = new Product();
                                product.setProductId(jsonArray.getJSONObject(x).getJSONArray("productDetails").getJSONObject(0).getString("product_id"));
                                product.setProductName(jsonArray.getJSONObject(x).getJSONArray("productDetails").getJSONObject(0).getString("product_name"));
                                product.setProductDescription(jsonArray.getJSONObject(x).getJSONArray("productDetails").getJSONObject(0).getString("product_description"));
                                product.setProductPrice(jsonArray.getJSONObject(x).getJSONArray("productDetails").getJSONObject(0).getString("product_price"));
                                product.setProductImage(jsonArray.getJSONObject(x).getJSONArray("productDetails").getJSONObject(0).getString("product_image"));
                                product.setProductCategory(jsonArray.getJSONObject(x).getJSONArray("productDetails").getJSONObject(0).getString("product_category"));
                                product.setProductDiscountPercentage(jsonArray.getJSONObject(x).getJSONArray("productDetails").getJSONObject(0).getString("product_discount_percentage"));
                                product.setProductSubCategory(jsonArray.getJSONObject(x).getJSONArray("productDetails").getJSONObject(0).getString("product_sub_category"));
                                mostBuyProducts.add(product);
                            }


                            mostBuyProductdapter = new MostBuyProductdapter(mContext, mostBuyProducts);
                            mostBuyProductList.setAdapter(mostBuyProductdapter);
                            loading.setVisibility(View.GONE);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e("Most Buy Products", "SOMETHING WENT WRONG!");
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.e("Most Buy Products", t.getMessage());

                }
            });
        } else {
            Utills.dialogBox(getString(R.string.network_tittle), getString(R.string.network_message), mContext);
        }
    }
}