package com.example.customerapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.customerapp.Adapter.ProductAdapter;
import com.example.customerapp.Adapter.SubCategorySpinAdapter;
import com.example.customerapp.Model.Category;
import com.example.customerapp.Model.Product;
import com.example.customerapp.Model.SubCategory;
import com.example.customerapp.OnClick.ProductClick;
import com.example.customerapp.Remote.RetrofitClient;
import com.example.customerapp.Service.IpService;
import com.example.customerapp.Utills.Utills;
import com.example.customerapp.database.DBUtils;
import com.google.gson.JsonObject;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuActivity extends AppCompatActivity implements ProductClick{

    Context mContext;
    Activity mActivity;
    Spinner spinnerSubSpinner, spinner;
    IpService mService;
    ArrayList<SubCategory> subCategoryArrayList;
    ArrayList<Category> categoryArrayList;
    ProductAdapter productAdapter;
    RecyclerView productRecycle;
    ArrayList<Product> productArrayList;
    ProductClick productClick;
    LinearLayout loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        mContext = this;
        mActivity = this;
        productClick = this;

        spinner = findViewById(R.id.spinner);
        spinnerSubSpinner = findViewById(R.id.spinnerSubCategory);
        productRecycle = findViewById(R.id.product_grid_view);
        loading = findViewById(R.id.loading);

        mService = new RetrofitClient().getClient(this).create(IpService.class);

        Toolbar categoryToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(categoryToolbar);
//        setTitle("Menu");

        AccountHeader headerResult = Utills.buildHeader(true, savedInstanceState, mActivity, mActivity);
        Drawer result = Utills.drawerFront1(categoryToolbar, savedInstanceState, mActivity, mActivity);

        final ArrayList<Category> category = DBUtils.getCategory(mContext);
        ArrayList<String> categoryName = new ArrayList<>();
        for (int x = 0; x < category.size(); x++) {
            categoryName.add(category.get(x).getCategoryName());
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(mContext, R.layout.drop_down_item, categoryName);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                final ArrayList<SubCategory> subCategoryArrayList = DBUtils.getSubcategoryBYCategoryId(mContext, category.get(i).getCategoryId());
                final ArrayList<String> subcategorySpin = new ArrayList<>();
                for (SubCategory sub : subCategoryArrayList) {
                    subcategorySpin.add(sub.getSubCategoryName());
                }
                SubCategorySpinAdapter subCategorySpinAdapter = new SubCategorySpinAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, subcategorySpin);
                spinnerSubSpinner.setAdapter(subCategorySpinAdapter);
                spinnerSubSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        HashMap<String, String> stringHashMap = new HashMap<>();
                        stringHashMap.put("product_sub_category", subCategoryArrayList.get(i).getSubCategoryId());

                        mService.getProductBySubCatId(stringHashMap).enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                if (response.code() == 200) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                                        productArrayList = new ArrayList<>();

                                        for (int x = 0; x < jsonArray.length(); x++) {
                                            Product product = new Product();
                                            product.setProductId(String.valueOf(jsonArray.getJSONObject(x).get("product_id")));
                                            product.setProductName(String.valueOf(jsonArray.getJSONObject(x).get("product_name")));
                                            product.setProductDescription(String.valueOf(jsonArray.getJSONObject(x).get("product_description")));
                                            product.setProductPrice(String.valueOf(jsonArray.getJSONObject(x).get("product_price")));
                                            product.setProductImage(String.valueOf(jsonArray.getJSONObject(x).get("product_image")));
                                            product.setProductCategory(String.valueOf(jsonArray.getJSONObject(x).get("product_category")));
                                            product.setProductSubCategory(String.valueOf(jsonArray.getJSONObject(x).get("product_sub_category")));
                                            product.setProductDiscountPercentage(String.valueOf(jsonArray.getJSONObject(x).get("product_discount_percentage")));
                                            product.setPromotion(String.valueOf(jsonArray.getJSONObject(x).get("promotion")));
                                            product.setProductAvailability(Integer.valueOf(String.valueOf(jsonArray.getJSONObject(x).get("product_availability"))));
                                            productArrayList.add(product);
                                        }
                                        if (productArrayList.size() != 0) {
                                            productRecycle.setLayoutManager(new GridLayoutManager(mContext, 2));

                                            productAdapter = new ProductAdapter(mContext, productArrayList, productClick);
                                            productRecycle.setAdapter(productAdapter);
                                            productAdapter.notifyDataSetChanged();

                                        } else {
                                            Toast.makeText(mContext, "There is no Products", Toast.LENGTH_SHORT).show();
                                            loading.setVisibility(View.VISIBLE);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Log.e("TAG", "SOMETHING WENT WRONG!");
                                }

                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {

                            }
                        });


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onProductItemClick(int position, String product) {
        Intent intent = new Intent(mContext, ProductViewActivity.class);
        intent.putExtra("productId", product);
        startActivity(intent);
    }
}