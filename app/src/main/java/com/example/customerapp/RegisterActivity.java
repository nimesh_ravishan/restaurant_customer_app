package com.example.customerapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.example.customerapp.Model.Customer;
import com.example.customerapp.Remote.RetrofitClient;
import com.example.customerapp.Service.IpService;
import com.example.customerapp.Utills.Utills;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    Activity mActivity;
    EditText firstName, lastName, email, address, streetAddress, mobile, password, confirmPassword;
    Context mContext;
    ImageView profileImage;
    private ProgressBar imgProgress;
    RelativeLayout imageLayout;
    IpService mService;
    Customer customer;
    LinearLayout userProfileContent, profileContentLoading;
    RelativeLayout profileImageLayout;
    FloatingActionButton editImageBtn;
    private static final int GALLERY_REQUEST_CODE = 838;
    private Uri selectedImage;
    Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mActivity = this;
        mContext = this;
        mService = new RetrofitClient().getClient(this).create(IpService.class);

        Toolbar orderDisplayToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(orderDisplayToolbar);
        setTitle("Profile");

        LinearLayout profileContent = findViewById(R.id.linLay3);

        AccountHeader headerResult = Utills.buildHeader(true, savedInstanceState, mActivity, mActivity);
        Drawer result = Utills.drawerFront1(orderDisplayToolbar, savedInstanceState, mActivity, mActivity);

        firstName = findViewById(R.id.firstName);
        lastName = findViewById(R.id.lastName);
        email = findViewById(R.id.email);
        mobile = findViewById(R.id.mobile);
        password = findViewById(R.id.password);
        confirmPassword = findViewById(R.id.confirmPassword);
        streetAddress = findViewById(R.id.streetAddress);
        address = findViewById(R.id.address);
        profileImage = findViewById(R.id.profileImage);
        imgProgress = findViewById(R.id.img_progress);
        imageLayout = findViewById(R.id.profileImageLayout);
        profileContentLoading = findViewById(R.id.profileContentLoading);
        userProfileContent = findViewById(R.id.profileContentLoading);
        profileImageLayout = findViewById(R.id.profileImageLayout);
        editImageBtn = findViewById(R.id.editImageBtn);
        register = findViewById(R.id.registerProfile);


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(firstName.getText().toString())) {
                    firstName.setError(getString(R.string.first_name_required));
                    firstName.requestFocus();
                } else if (TextUtils.isEmpty(lastName.getText().toString())) {
                    lastName.setError(getString(R.string.last_name_required));
                    lastName.requestFocus();
                } else if (TextUtils.isEmpty(email.getText().toString())) {
                    email.setError(getString(R.string.email_required));
                    email.requestFocus();
                } else if (TextUtils.isEmpty(mobile.getText().toString())) {
                    mobile.setError(getString(R.string.mobile_required));
                    mobile.requestFocus();
                } else if (TextUtils.isEmpty(password.getText().toString())) {
                    password.setError(getString(R.string.password_required));
                    password.requestFocus();
                } else if (TextUtils.isEmpty(confirmPassword.getText().toString())) {
                    confirmPassword.setError(getString(R.string.confirm_password_required));
                    confirmPassword.requestFocus();
                } else if (!confirmPassword.getText().toString().equalsIgnoreCase(password.getText().toString())) {
                    confirmPassword.setError(getString(R.string.Password_confirm_password_required));
                    confirmPassword.requestFocus();
                } else if (TextUtils.isEmpty(streetAddress.getText().toString())) {
                    streetAddress.setError(getString(R.string.street_address_required));
                    streetAddress.requestFocus();
                } else if (TextUtils.isEmpty(address.getText().toString())) {
                    address.setError(getString(R.string.address_required));
                    address.requestFocus();
                } else {
                    Utills.progressBar(mContext);

                    HashMap<String, String> stringHashMap = new HashMap<>();
                    stringHashMap.put("first_name", firstName.getText().toString());
                    stringHashMap.put("last_name", lastName.getText().toString());
                    stringHashMap.put("email", email.getText().toString());
                    stringHashMap.put("streat_address", streetAddress.getText().toString());
                    stringHashMap.put("address", address.getText().toString());
                    stringHashMap.put("mobile", mobile.getText().toString());
                    stringHashMap.put("password", password.getText().toString());
                    stringHashMap.put("user_role", "4");
                    stringHashMap.put("is_block_user", "0");

                    mService.registerUser(stringHashMap).enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            if (response.code() == 200) {
                                Utills.progresBarDismiss();
                                if (response.body() != null) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(response.body().toString());
                                        SharedPreferences userPref = getApplicationContext().getSharedPreferences(getApplicationContext().getResources().getString(R.string.PREF_AUTH_TITLE), Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editors = userPref.edit();
                                        editors.putBoolean(getApplicationContext().getResources().getString(R.string.IS_PREF_AUTH_LOGGED), true);
                                        String userName = jsonObject.getJSONArray("data").getJSONObject(0).getString("first_name") + jsonObject.getJSONArray("data").getJSONObject(0).getString("last_name");
                                        editors.putString(getApplicationContext().getResources().getString(R.string.PREF_AUTH_USER_ID), jsonObject.getJSONArray("data").getJSONObject(0).getString("user_id"));
                                        editors.putString(getApplicationContext().getResources().getString(R.string.PREF_AUTH_EMAIL), jsonObject.getJSONArray("data").getJSONObject(0).getString("email"));
                                        editors.putString(getApplicationContext().getResources().getString(R.string.PREF_AUTH_USERNAME), userName);
                                        editors.apply();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    Log.e("RESPONSEBODY", "onResponse: " + response.body());
                                }
                                Intent intent = new Intent(mContext, HomePageActivity.class);
                                startActivity(intent);
                                finish();

                            } else {
                                if (response.message() != null) {
                                    Utills.progresBarDismiss();
                                    Utills.dialogBox(getString(R.string.error_title), response.message(), mContext);
                                }
                            }


                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            Utills.progresBarDismiss();
                            Log.e("**LOGIN**", "onFailure: " + t);

                        }
                    });
                }
            }
        });


    }
}