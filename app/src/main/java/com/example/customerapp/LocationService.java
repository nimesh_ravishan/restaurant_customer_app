package com.example.customerapp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.GridLayoutManager;

import com.example.customerapp.Adapter.PromotionAdapter;
import com.example.customerapp.Model.Promotion;
import com.example.customerapp.Model.Restaurant;
import com.example.customerapp.Remote.RetrofitClient;
import com.example.customerapp.Service.IpService;
import com.example.customerapp.Utills.Utills;
import com.example.customerapp.database.DBUtils;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationService extends Service {

    private static final String TAG = "LOCATIONGPS";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;
    String channelId = "TEST_CHANNEL";
    IpService mService;
    ArrayList<Promotion> promotionArrayList;
    boolean isInCirculer = false;

    private class LocationListner implements LocationListener {


        Location mLastLocation;

        public LocationListner(String provider) {
            Log.e(TAG, "LocationListener :: " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.e(TAG, "Location changed:: " + location);
            mLastLocation.set(location);

            double longitude = location.getLongitude();
            double latitude = location.getLatitude();

            if (PointIsInRegion(latitude, longitude, null)) {
                Log.i(" GEO SERVICE : ", " in the circle");
                if (!isInCirculer) {
                    isInCirculer = true;
                    if (!Utills.isNetworkNotAvailable(getApplicationContext())) {
                        mService.getPromotion().enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                if (response.code() == 200) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                                        promotionArrayList = new ArrayList<>();

                                        for (int x = 0; x < jsonArray.length(); x++) {
                                            Promotion promotion = new Promotion();
                                            promotion.setPromotionId(String.valueOf(jsonArray.getJSONObject(x).get("promotion_id")));
                                            promotion.setPromoProductId(String.valueOf(jsonArray.getJSONObject(x).get("product_id")));
                                            promotion.setOfferPercentage(String.valueOf(jsonArray.getJSONObject(x).get("offer_percentage")));
                                            promotion.setPromotionDescription(String.valueOf(jsonArray.getJSONObject(x).get("promotion_description")));
                                            promotion.setPromotionActivation(Integer.valueOf(String.valueOf(jsonArray.getJSONObject(x).get("activate_promotion"))));

                                            if (String.valueOf(jsonArray.getJSONObject(x).get("activate_promotion")).equalsIgnoreCase("1")) {
                                                promotionArrayList.add(promotion);
                                            }
                                        }
                                        for (int i = 0; i < promotionArrayList.size(); i++) {
                                            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), channelId)
                                                    .setSmallIcon(R.drawable.ic_launcher_background)
                                                    .setContentTitle("RESTAURANT OFFERS")
                                                    .setContentText(promotionArrayList.get(i).getPromotionDescription())
                                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);

                                            createNotificationChannel();
                                            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
                                            notificationManager.notify(i, builder.build());


                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Utills.dialogBox(getString(R.string.app_name), getString(R.string.error_message), getApplicationContext());
                                    Log.e("PROMOTION", "SOMETHING WENT WRONG!");
                                }
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                Utills.dialogBox(getString(R.string.app_name), t.getMessage(), getApplicationContext());

                            }
                        });

                    }

                }

            } else {
                Log.i(" GEO SERVICE : ", " out of the circle");
                isInCirculer = false;

            }
        }

        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled :: " + provider);
        }

        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListner(LocationManager.GPS_PROVIDER),
            new LocationListner(LocationManager.NETWORK_PROVIDER)
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        onTaskRemoved(intent);
        super.onStartCommand(intent, flags, startId);
        return Service.START_STICKY;
    }


    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        initializeLocationManager();
        mService = new RetrofitClient().getClient(this).create(IpService.class);

        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    (android.location.LocationListener) mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    (android.location.LocationListener) mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates((android.location.LocationListener) mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());

        if (Build.VERSION.SDK_INT >= 26) {
            String CHANNEL_ID = "my_channel_01";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);

            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("")
                    .setContentText("").build();

            startForeground(1, notification);
        } else
            startService(restartServiceIntent);
        super.onTaskRemoved(rootIntent);
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "CHANNEL_NAME";
            String description = "CHANNEL_DESCRIPTION";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(channelId, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
//    check the polygon

    class LatLng {
        double Latitude;
        double Longitude;

        LatLng(double lat, double lon) {
            Latitude = lat;
            Longitude = lon;
        }
    }

    public boolean PointIsInRegion(double x, double y, LatLng[] thePath) {

        thePath = new LatLng[4];

        Restaurant restaurant = DBUtils.getRestaurant(getApplicationContext());
        thePath[0] = new LatLng(Double.parseDouble(restaurant.getLatLong1().substring(0,restaurant.getLatLong1().indexOf(",")-1)),
                Double.parseDouble(restaurant.getLatLong1().substring(restaurant.getLatLong1().lastIndexOf(",")+1)));


        thePath[1] = new LatLng(Double.parseDouble(restaurant.getLatLong2().substring(0,restaurant.getLatLong2().indexOf(",")-1)),
                Double.parseDouble(restaurant.getLatLong2().substring(restaurant.getLatLong2().lastIndexOf(",")+1)));
        thePath[2] = new LatLng(Double.parseDouble(restaurant.getLatLong3().substring(0,restaurant.getLatLong3().indexOf(",")-1)),
                Double.parseDouble(restaurant.getLatLong3().substring(restaurant.getLatLong3().lastIndexOf(",")+1)));
        thePath[3] = new LatLng(Double.parseDouble(restaurant.getLatLong4().substring(0,restaurant.getLatLong4().indexOf(",")-1)),
                Double.parseDouble(restaurant.getLatLong4().substring(restaurant.getLatLong4().lastIndexOf(",")+1)));

        try {
            int crossings = 0;

            LatLng point = new LatLng(x, y);
            int count = thePath.length;

            // for each edge
            for (int i = 0; i < count; i++) {
                LatLng a = thePath[i];
                int j = i + 1;
                if (j >= count) {
                    j = 0;
                }
                LatLng b = thePath[j];
                if (RayCrossesSegment(point, a, b)) {
                    crossings++;
                }
            }
            // odd number of crossings?
            return (crossings % 2 == 1);

        } catch (Exception e) {
            Log.e(" GEO SERVICE : ", " something went wrong !!!!");
            return false;
        }

    }

    public boolean RayCrossesSegment(LatLng point, LatLng a, LatLng b) {
        Double px = point.Longitude;
        Double py = point.Latitude;
        Double ax = a.Longitude;
        Double ay = a.Latitude;
        Double bx = b.Longitude;
        Double by = b.Latitude;
        if (ay > by) {
            ax = b.Longitude;
            ay = b.Latitude;
            bx = a.Longitude;
            by = a.Latitude;
        }

        // alter longitude to cater for 180 degree crossings
        if (px < 0) {
            px += 360;
        }
        ;
        if (ax < 0) {
            ax += 360;
        }
        ;
        if (bx < 0) {
            bx += 360;
        }
        ;

        if (py == ay || py == by) py += 0.00000001;
        if ((py > by || py < ay) || (px > Math.max(ax, bx))) return false;
        if (px < Math.min(ax, bx)) return true;

        double red = (ax != bx) ? ((by - ay) / (bx - ax)) : Float.MAX_VALUE;
        double blue = (ax != px) ? ((py - ay) / (px - ax)) : Float.MAX_VALUE;
        return (blue >= red);
    }

}
