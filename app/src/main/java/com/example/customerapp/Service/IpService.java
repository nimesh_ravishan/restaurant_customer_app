package com.example.customerapp.Service;

import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface IpService {

    //Login
    @FormUrlEncoded
    @POST("Auth/adminLogin")
    Call<JsonObject> signUser(@FieldMap Map<String, String> params);

    @GET("category/getCategory")
    Call<JsonObject> getCategory();

    @GET("product/getProduct")
    Call<JsonObject> getProducts();

    @GET("promotion/getPromotion")
    Call<JsonObject> getPromotion();

    @FormUrlEncoded
    @POST("order/insertOrder")
    Call<JsonObject> insertOrders(@FieldMap Map<String, String> params);

    @POST("order/getOrderProductByOrderDate")
    Call<JsonObject> getOrderByDate();

    @FormUrlEncoded
    @POST("product/insertRatings")
    Call<JsonObject> addRate(@FieldMap Map<String, String> params);

    @GET("order/getmostBuyProducts")
    Call<JsonObject> getMostBuyProducts();

    @GET("Auth/getRestaurant")
    Call<JsonObject> getRestaurant();

    @FormUrlEncoded
    @POST("product/getProductBySubCategoryId")
    Call<JsonObject> getProductBySubCatId(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("product/getProductById")
    Call<JsonObject> getProductById(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("order/getOrderProductByCustomerId")
    Call<JsonObject> getOrdersByUserId(@FieldMap Map<String,String> params);

    @FormUrlEncoded
    @POST("Customer/getCustomerById")
    Call<JsonObject> getCustomerById(@FieldMap Map<String,String> params);

    @FormUrlEncoded
    @POST("Customer/updateCustomer")
    Call<JsonObject> updateUser(@FieldMap Map<String,String> params);

    @Multipart
    @POST("Customer/insertCustomerImage")
    Call<JsonObject> insertProfileImage(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part file, @Part("file") RequestBody name);

    @FormUrlEncoded
    @POST("Customer/insertCustomer")
    Call<JsonObject> registerUser(@FieldMap Map<String,String> params);


}
