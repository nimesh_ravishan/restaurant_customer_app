package com.example.customerapp;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.customerapp.Adapter.CartAdapter;
import com.example.customerapp.Adapter.ProductAdapter;
import com.example.customerapp.Adapter.SubCategorySpinAdapter;
import com.example.customerapp.Model.CartObject;
import com.example.customerapp.Model.Category;
import com.example.customerapp.Model.Orders;
import com.example.customerapp.Model.Product;
import com.example.customerapp.Model.SubCategory;
import com.example.customerapp.OnClick.CartProductRemove;
import com.example.customerapp.OnClick.ProductClick;
import com.example.customerapp.Remote.RetrofitClient;
import com.example.customerapp.Service.IpService;
import com.example.customerapp.Utills.Utills;
import com.example.customerapp.database.DBUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.JsonObject;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderPlaceActivity extends AppCompatActivity implements ProductClick, CartProductRemove {

    Context mContext;
    Spinner spinnerSubSpinner, spinner;
    RecyclerView productRecycle;
    ProductAdapter productAdapter;
    ProductClick productClick;
    ArrayList<Product> productArrayList;
    ArrayList<CartObject> cartObjects = new ArrayList<>();
    private CartAdapter mCartAdapter;
    private RecyclerView mCartListView;
    TextView total, pay;
    private double totalPrice;
    IpService mService;
    CartProductRemove cartProductRemove;
    JSONArray jsonArray;
    LinearLayout orderPayment;
    private FirebaseDatabase mDatabase;
    private DatabaseReference mDbRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_place);
        mDatabase = FirebaseDatabase.getInstance();
        mDbRef = mDatabase.getReference("Orders");

        totalPrice = 0.00;

        mContext = this;
        productClick = this;
        cartProductRemove = this;
        final Activity mActivity = OrderPlaceActivity.this;
        mService = new RetrofitClient().getClient(this).create(IpService.class);

        mDbRef.addChildEventListener(childListener);

        spinner = findViewById(R.id.spinner);
        spinnerSubSpinner = findViewById(R.id.spinnerSubCategory);
        productRecycle = findViewById(R.id.product_grid_view);
        mCartListView = findViewById(R.id.cart_list_view);
        mCartListView.setLayoutManager(new LinearLayoutManager(mContext));
        total = findViewById(R.id.total_price);
        pay = findViewById(R.id.pay);

        orderPayment = findViewById(R.id.order_payment);
        LinearLayout orderPaymentCash = findViewById(R.id.cash);
        LinearLayout orderPaymentCard = findViewById(R.id.card);


        Toolbar categoryToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(categoryToolbar);
        setTitle("Create Orders");

        AccountHeader headerResult = Utills.buildHeader(true, savedInstanceState, mActivity, mActivity);
        Drawer result = Utills.drawerFront1(categoryToolbar, savedInstanceState, mActivity, mActivity);

        final ArrayList<Category> category = DBUtils.getCategory(mContext);
        ArrayList<String> categoryName = new ArrayList<>();
        for (int x = 0; x < category.size(); x++) {
            categoryName.add(category.get(x).getCategoryName());
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(mContext, R.layout.drop_down_item, categoryName);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                final ArrayList<SubCategory> subCategoryArrayList = DBUtils.getSubcategoryBYCategoryId(OrderPlaceActivity.this, category.get(i).getCategoryId());
                final ArrayList<String> subcategorySpin = new ArrayList<>();
                for (SubCategory sub : subCategoryArrayList) {
                    subcategorySpin.add(sub.getSubCategoryName());
                }
                SubCategorySpinAdapter subCategorySpinAdapter = new SubCategorySpinAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, subcategorySpin);
                spinnerSubSpinner.setAdapter(subCategorySpinAdapter);
                spinnerSubSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        HashMap<String, String> stringHashMap = new HashMap<>();
                        stringHashMap.put("product_sub_category", subCategoryArrayList.get(i).getSubCategoryId());

                        mService.getProductBySubCatId(stringHashMap).enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                if (response.code() == 200) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                                        productArrayList = new ArrayList<>();

                                        for (int x = 0; x < jsonArray.length(); x++) {
                                            Product product = new Product();
                                            product.setProductId(String.valueOf(jsonArray.getJSONObject(x).get("product_id")));
                                            product.setProductName(String.valueOf(jsonArray.getJSONObject(x).get("product_name")));
                                            product.setProductDescription(String.valueOf(jsonArray.getJSONObject(x).get("product_description")));
                                            product.setProductPrice(String.valueOf(jsonArray.getJSONObject(x).get("product_price")));
                                            product.setProductImage(String.valueOf(jsonArray.getJSONObject(x).get("product_image")));
                                            product.setProductCategory(String.valueOf(jsonArray.getJSONObject(x).get("product_category")));
                                            product.setProductSubCategory(String.valueOf(jsonArray.getJSONObject(x).get("product_sub_category")));
                                            product.setProductDiscountPercentage(String.valueOf(jsonArray.getJSONObject(x).get("product_discount_percentage")));
                                            product.setPromotion(String.valueOf(jsonArray.getJSONObject(x).get("promotion")));
                                            product.setProductAvailability(Integer.valueOf(String.valueOf(jsonArray.getJSONObject(x).get("product_availability"))));
                                            productArrayList.add(product);
                                        }
                                        if (productArrayList.size() != 0) {
                                            LinearLayoutManager layoutManager
                                                    = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                                            productRecycle.setLayoutManager(layoutManager);

                                            productAdapter = new ProductAdapter(mContext, productArrayList, productClick);
                                            productRecycle.setAdapter(productAdapter);
                                            productAdapter.notifyDataSetChanged();


                                        } else {
                                            Toast.makeText(mContext, "There is no Products", Toast.LENGTH_SHORT).show();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Log.e("TAG", "SOMETHING WENT WRONG!");
                                }

                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {

                            }
                        });

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cartObjects.size() > 0) {
                    orderPayment.setVisibility(View.VISIBLE);
                }

            }
        });

        orderPayment
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        orderPayment.setVisibility(View.GONE);
                    }
                });

        orderPaymentCash
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        orderPayment.setVisibility(View.GONE);

                        insertOrder("1");
                    }
                });

        orderPaymentCard
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        orderPayment.setVisibility(View.GONE);

                        insertOrder("2");
                    }
                });

    }

    ChildEventListener childListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            Log.e("FIREBASE", "onChildAdded: " + dataSnapshot.getChildrenCount());
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            Log.e("FIREBASE", "onChildChanged: ");
            Orders orders = dataSnapshot.getValue(Orders.class);

            String orderStatus = orders.getOrder_status();

            if(Utills.getUserID(mContext).equalsIgnoreCase(orders.getUser_id())) {
                if(orderStatus.equalsIgnoreCase("2")) {
                    showOrderPreparing(orderStatus);

                }else if(orderStatus.equalsIgnoreCase("3")){
                    showOrderReady(orderStatus);

                }
            }
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            Log.e("FIREBASE", "onChildRemoved: ");
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
    public void insertOrder(String paymentType) {
        final HashMap<String, String> stringHashMap = new HashMap<>();
        stringHashMap.put("table", "01");
        stringHashMap.put("user_id", Utills.getUserID(mContext));
        stringHashMap.put("payment_type", paymentType);
        stringHashMap.put("order_type", "1");
        stringHashMap.put("order_status", "1");

        jsonArray = new JSONArray();
        for (CartObject cartObject : cartObjects) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("cartProductComment", cartObject.getCartProductComment());
                jsonObject.put("cartProductId", cartObject.getCartProductId());
                jsonObject.put("cartProductName", cartObject.getCartProductName());
                jsonObject.put("cartProductOffer", cartObject.getCartProductOffer());
                jsonObject.put("cartProductPrice", cartObject.getCartProductPrice());
                jsonObject.put("cartProductQuantity", cartObject.getCartProductQuantity());
                jsonObject.put("cartProductImage", cartObject.getCartProductImage());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }
        stringHashMap.put("products", jsonArray.toString());

        mService.insertOrders(stringHashMap).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.e("INSERT ORDER", "onResponse: " + response.code() + "||" + response.body());

                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                    String orderId = jsonObject.getString("orderId");

                    if (!orderId.equalsIgnoreCase("")) {

                        mDbRef.child(orderId).setValue(stringHashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Log.d("ORDER", "isSuccess: " + " added firebase");
                                    Toast.makeText(mContext, "Order Added Successfully", Toast.LENGTH_SHORT).show();

                                } else {
                                    Log.d("ORDER", "is not success: " + " not added firebase");

                                }
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e("INSERT ORDER", "onFailure: " + t.getMessage());
            }
        });

    }
    public void updateTotalPrice() {
        totalPrice = 0.00;
        for (int i = 0; i < cartObjects.size(); i++) {
            CartObject object = cartObjects.get(i);
            totalPrice += object.getCartProductQuantity() * object.getCartProductPrice();
        }
        total.setText(String.valueOf(totalPrice));
    }

    public boolean isProductAvailable(String productId) {
        if (!productId.equalsIgnoreCase("")) {
            for (int i = 0; i < cartObjects.size(); i++) {
                String proId = cartObjects.get(i).getCartProductId();
                if (proId == productId) {
                    return true;
                }
            }
        }
        return false;
    }

    public CartObject getProductQuantity(String productId) {
        if (!productId.equalsIgnoreCase("")) {
            for (int i = 0; i < cartObjects.size(); i++) {
                String proId = cartObjects.get(i).getCartProductId();
                if (proId == productId) {
                    return cartObjects.get(i);
                }
            }
        }
        return null;
    }


    public void showOrderReady(String orderStatus){


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("ORDER");
        final View customLayout = getLayoutInflater().inflate(R.layout.custom_dialogbox_layout, null);
        builder.setView(customLayout);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public void showOrderPreparing(String orderStatus){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("ORDER");
        final View customLayout = getLayoutInflater().inflate(R.layout.custom_preparing_dialogbox_layout, null);
        builder.setView(customLayout);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    @Override
    public void onProductRemove(int position) {
        CartObject object = cartObjects.get(position);
        double totalMinPrice = object.getCartProductQuantity() * object.getCartProductPrice();
        totalPrice -= totalMinPrice;

        total.setText(String.valueOf(totalPrice));

    }

    @Override
    public void onProductItemClick(int position, String product) {
        int quantity = 0;
        int offer = 0;
        Product mProductDetail = productArrayList.get(position);

        if (!mProductDetail.getPromotion().equalsIgnoreCase("[]")) {
            try {
                JSONArray jsonArray = new JSONArray(mProductDetail.getPromotion());
                for (int i = 0; i < jsonArray.length(); i++) {
                    int promotionStatus = Integer.parseInt(jsonArray.getJSONObject(i).getString("activate_promotion"));

                    if (promotionStatus != 0) {
                        offer = Integer.parseInt(jsonArray.getJSONObject(i).getString("offer_percentage"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


        CartObject cartObject = new CartObject();

        CartObject cartQuantityObj = getProductQuantity(product);

        if (cartQuantityObj == null) {
            quantity = 1;
        } else {
            quantity = cartQuantityObj.getCartProductQuantity() + 1;
        }

        float cartItemPrice = Float.parseFloat(mProductDetail.getProductPrice());

        cartObject.setCartProductId(mProductDetail.getProductId());
        cartObject.setCartProductName(mProductDetail.getProductName());
        cartObject.setCartProductQuantity(quantity);
        cartObject.setCartProductPrice(cartItemPrice - Float.valueOf( cartItemPrice/ 100.0f) * offer);
        cartObject.setCartProductOffer("");
        cartObject.setCartProductComment("");
        cartObject.setCartProductImage(mProductDetail.getProductImage());

        if (isProductAvailable(product)) {
            for (int i = 0; i < cartObjects.size(); i++) {
                String proId = cartObjects.get(i).getCartProductId();
                if (proId == product) {
                    cartObjects.set(i, cartObject);

                }
            }
        } else {
            cartObjects.add(cartObject);
        }


        mCartAdapter = new CartAdapter(mContext, cartObjects, cartProductRemove);
        mCartListView.setAdapter(mCartAdapter);
        updateTotalPrice();
    }
}