package com.example.customerapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.customerapp.Adapter.RatingListAdapter;
import com.example.customerapp.Model.Product;
import com.example.customerapp.Model.Rating;
import com.example.customerapp.Remote.RetrofitClient;
import com.example.customerapp.Service.IpService;
import com.example.customerapp.Utills.Utills;
import com.example.customerapp.database.DBUtils;
import com.google.gson.JsonObject;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductViewActivity extends AppCompatActivity {
    Activity mActivity;
    Context mContext;
    ImageView productImage;
    TextView productName, productPrice, productDescription, offer,ratingCount,ratingUserCount;
    ProgressBar imgProgress;
    RatingBar ratingBar;
    float allRatingCount;
    int usersCount;
    RecyclerView ratingListItem;
    RatingListAdapter ratingListAdapter;
    ArrayList<Rating> ratings;
    IpService mService;
    Product product;
    String objectName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_view);

        productImage = findViewById(R.id.productViewImage);
        productName = findViewById(R.id.productName);
        productPrice = findViewById(R.id.productPrice);
        productDescription = findViewById(R.id.productDescription);
        offer = findViewById(R.id.offer);
        imgProgress = findViewById(R.id.img_progress);
        ratingCount = findViewById(R.id.ratingCount);
        ratingUserCount = findViewById(R.id.ratingUserCount);
        ratingBar = findViewById(R.id.allRatingCount);
        ratingListItem = findViewById(R.id.rating_list);


        mContext = this;
        mActivity = ProductViewActivity.this;
        mService = new RetrofitClient().getClient(this).create(IpService.class);

        Intent intent = getIntent();
        String productId = intent.getStringExtra("productId");

        Toolbar productToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(productToolbar);
        setTitle(DBUtils.getProductNameByProductId(mContext,productId));

        AccountHeader headerResult = Utills.buildHeader(true, savedInstanceState, mActivity, mActivity);
        Drawer result = Utills.drawerFront1(productToolbar, savedInstanceState, mActivity, mActivity);

        HashMap<String, String> stringHashMap = new HashMap<>();
        stringHashMap.put("product_id", productId);


        mService.getProductById(stringHashMap).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(response.body()));
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for(int x = 0; x<jsonArray.length(); x++){
                             product = new Product();
                            product.setProductId(String.valueOf(jsonArray.getJSONObject(x).get("product_id")));
                            product.setProductName(String.valueOf(jsonArray.getJSONObject(x).get("product_name")));
                            product.setProductDescription(String.valueOf(jsonArray.getJSONObject(x).get("product_description")));
                            product.setProductPrice(String.valueOf(jsonArray.getJSONObject(x).get("product_price")));
                            product.setProductImage(String.valueOf(jsonArray.getJSONObject(x).get("product_image")));
                            product.setProductCategory(String.valueOf(jsonArray.getJSONObject(x).get("product_category")));
                            product.setProductSubCategory(String.valueOf(jsonArray.getJSONObject(x).get("product_sub_category")));
                            product.setProductDiscountPercentage(String.valueOf(jsonArray.getJSONObject(x).get("product_discount_percentage")));
                            product.setPromotion(String.valueOf(jsonArray.getJSONObject(x).get("promotion")));
                            product.setRatings(String.valueOf(jsonArray.getJSONObject(x).get("ratings")));
                            product.setProductAvailability(Integer.valueOf(String.valueOf(jsonArray.getJSONObject(x).get("product_availability"))));
                            objectName = String.valueOf(jsonArray.getJSONObject(x).get("object_name"));
                        }

                        viewProductDetails(product);




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("TAG", "SOMETHING WENT WRONG!");
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });




    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.add_items) {
            Log.e("ADD CATEGORY", "onOptionsItemSelected: ");

            if(!objectName.equalsIgnoreCase("")) {
                Intent intent = new Intent(ProductViewActivity.this, MainActivity.class);
                intent.putExtra("objectName", objectName);
                startActivity(intent);
            }else{
                Toast.makeText(mActivity, "There is no AR objects insert!", Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void viewProductDetails(Product product){

        if (!product.getRatings().equalsIgnoreCase("[]")) {

            ratings = new ArrayList<>();
            try {
                JSONArray jsonArray = new JSONArray(product.getRatings());
                for (int i = 0; i < jsonArray.length(); i++) {
                    usersCount += 1;
                    String ratingCount = jsonArray.getJSONObject(i).getString("rate_count");
                    if(!ratingCount.equalsIgnoreCase("0")) {
                        allRatingCount += Float.valueOf(ratingCount);
                    }
                    Rating rating = new Rating();
                    rating.setRatingId(jsonArray.getJSONObject(i).getString("rating_id"));
                    rating.setProductId(jsonArray.getJSONObject(i).getString("product_id"));
                    rating.setUserId(jsonArray.getJSONObject(i).getString("user_id"));
                    rating.setRateComment(jsonArray.getJSONObject(i).getString("rate_comment"));
                    rating.setRateDateAndTime(jsonArray.getJSONObject(i).getString("date_and_time"));
                    rating.setRateCount(jsonArray.getJSONObject(i).getString("rate_count"));
                    rating.setUserName(jsonArray.getJSONObject(i).getString("userName"));
                    ratings.add(rating);

                }

                LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                ratingListItem.setLayoutManager(layoutManager);

                ratingListAdapter = new RatingListAdapter(mContext, ratings);
                ratingListItem.setAdapter(ratingListAdapter);

                float x = (float) (((double) allRatingCount / (double) Float.valueOf(usersCount)));

                ratingCount.setText(String.valueOf(x));
                ratingUserCount.setText(String.valueOf(usersCount));
                ratingBar.setRating(x);


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }



        if (!product.getPromotion().equalsIgnoreCase("[]")) {
            try {
                JSONArray jsonArray = new JSONArray(product.getPromotion());
                for (int i = 0; i < jsonArray.length(); i++) {
                    int promotionStatus = Integer.parseInt(jsonArray.getJSONObject(i).getString("activate_promotion"));

                    if (promotionStatus != 0) {
                        String offerCount = jsonArray.getJSONObject(i).getString("offer_percentage").toString();

                        offer.setVisibility(View.VISIBLE);
                        offer.setText(offerCount + "%");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        if (!product.getProductImage().equalsIgnoreCase("") && product.getProductImage() != null && !product.getProductImage().equalsIgnoreCase("null")) {
            productImage.setVisibility(View.GONE);

            Picasso.get().load(product.getProductImage())
                    .into(productImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            imgProgress.setVisibility(View.GONE);
                            productImage.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onError(Exception e) {
                            productImage.setVisibility(View.VISIBLE);

                        }

                    });


        } else {
            productImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.logo));
        }

        if (!product.getProductName().equalsIgnoreCase("") && product.getProductName() != null && !product.getProductName().equalsIgnoreCase("null")) {
            productName.setText(product.getProductName());
        }
        if (!product.getProductPrice().equalsIgnoreCase("") && product.getProductPrice() != null && !product.getProductPrice().equalsIgnoreCase("null")) {
            productPrice.setText(product.getProductPrice());
        }
        if (!product.getProductDescription().equalsIgnoreCase("") && product.getProductDescription() != null && !product.getProductDescription().equalsIgnoreCase("null")) {
            productDescription.setText(product.getProductDescription());
        }


    }

}